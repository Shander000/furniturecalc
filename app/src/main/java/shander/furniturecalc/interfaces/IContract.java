package shander.furniturecalc.interfaces;

import shander.furniturecalc.entities.BaseCatalogue;
import shander.furniturecalc.entities.Material;

public interface IContract {

    String CALLER_ACTIVITY = "caller";
    String FROM_PROJECT = "fromProject";
    String MATERIAL = "material";
    String NDS = "nds";
    String COEFF = "coeff";
    String CALCULATION = "calculation";
    String PROJECT = "project";
    String IS_EDIT = "isEdit";
    String CATALOGUE = "catalogue";
    String KEEP_SCREEN = "keep_screen";

    Integer PROJECT_NULL = 111;
    Integer CALCULATION_NULL = 222;
    Integer CATALOGUE_NULL = 333;

    String PREMIUM_ZERO = "premium0";

    String DB_NAME = "furniture_db";
    String TABLE_CATEGORIES = "table_categories";
    String TABLE_MATERIALS = "table_furn";
    String TABLE_CALCULATIONS = "table_calc";
    String TABLE_NOT_AVAILABLE_MATEIALS = "table_not_available";
    String TABLE_PROJECTS = "table_projects";

    String ID = "id";

    String TABLE_ID = "_id";
    String CATEGORY_NAME = "name";
    String[] ALL_CATEGORY_FIELDS = {TABLE_ID, CATEGORY_NAME};

    String MATERIAL_ARTICLE = "article";
    String MATERIAL_NAME = "material_name";
    String MATERIAL_COMMENT = "comment";
    String MATERIAL_PRICE = "price";
    String MATERIAL_UNIT = "unit";
    String MATERIAL_CATEGORY = "category";
    String[] ALL_MATERIAL_FIELDS = {TABLE_ID, MATERIAL_ARTICLE, MATERIAL_NAME, MATERIAL_COMMENT,
            MATERIAL_PRICE, MATERIAL_UNIT, MATERIAL_CATEGORY};

    String CALCULATION_TITLE = "calc_title";
    String CALCULATION_AMOUNT = "calc_amount";
    String CALCULATION_PROJECT_ID = "calc_parent_id";
    String CALCULATION_MATERIAL_ID = "calc_material_id";
    String CALCULATION_COMMENT = "calc_comment";
    String CALCULATION_IS_NDS = "is_nds";
    String CALCULATION_IS_PERCENT = "is_percent";
    String CALCULATION_NDS = "nds";
    String CALCULATION_PERCENTS = "percents";
    String MATERIAL_IS_AVAILABLE = "is_available";
    String[] ALL_CALCULATION_FIELDS = {TABLE_ID, CALCULATION_TITLE, CALCULATION_AMOUNT,
    CALCULATION_PROJECT_ID, CALCULATION_MATERIAL_ID, CALCULATION_COMMENT, CALCULATION_IS_NDS,
    CALCULATION_IS_PERCENT, CALCULATION_NDS, CALCULATION_PERCENTS, MATERIAL_IS_AVAILABLE};

    String PROJECT_NAME = "proj_name";
    String PROJECT_ADDRESS = "address";
    String PROJECT_FIO = "fio";
    String PROJECT_NOTE = "note";
    String PROJECT_PHONE = "phone";
    String[] ALL_PROJECT_FIELDS = {TABLE_ID, PROJECT_NAME, PROJECT_ADDRESS, PROJECT_FIO, PROJECT_NOTE, PROJECT_PHONE};

    String NOT_AVAILABLE_MATERIAL = "na_material_id";
    String NOT_AVAILABLE_AMOUNT = "na_amount";
    String NOT_AVAILABLE_PROJECT = "na_project";
    String[] ALL_NOT_AVAILABLE_FIELDS = {TABLE_ID, NOT_AVAILABLE_MATERIAL, NOT_AVAILABLE_AMOUNT, NOT_AVAILABLE_PROJECT};

    BaseCatalogue[] DEFAULT_CATEGORIES = {
            new BaseCatalogue("Листовой материал", 1),
            new BaseCatalogue("Погонный материал", 2),
            new BaseCatalogue("Кромочный материал", 3),
            new BaseCatalogue("Крепеж", 4),
            new BaseCatalogue("Фурнитура", 5),
            new BaseCatalogue("Работы", 6),
            new BaseCatalogue("Другое", 7)
    };

    Material[] DEFAULT_MATERIALS = {
            new Material("2", "", 0, 1400, "ЛДСП Вишня Оксфорд 2750х1830х16мм", 1, "лист"),
            new Material("3", "", 0, 1550, "ЛДСП Венге 2750х1830х16мм", 1, "лист"),
            new Material("4", "", 0, 1750, "ЛДСП Красный 2750х1830х16мм", 1, "лист"),
            new Material("5", "", 0, 1900, "ЛДСП Лайм 2750х1830х16мм", 1, "лист"),
            new Material("6", "", 0, 2300, "ЛДСП Дуб молочный 2750х1830х25мм", 1, "лист"),
            new Material("7", "", 0, 2500, "ЛДСП Дуб тортона 2750х1830х25 мм", 1, "лист"),
            new Material("8", "", 0, 2700, "ЛДСП Древесина Аттик 2750х1830х25 мм", 1, "лист"),
            new Material("9", "", 0, 2300, "МДФ Мокко 16 мм", 1, "кв.м."),
            new Material("10", "", 0, 2500, "МДФ Капучино 16 мм", 1, "кв.м."),
            new Material("11", "", 0, 2700, "МДФ Патина 16 мм", 1, "кв.м."),
            new Material("12", "", 0, 3000, "МДФ Ноче Мария Луиза 19 мм", 1, "кв.м."),
            new Material("13", "", 0, 3500, "МДФ Венге 28 мм", 1, "кв.м."),
            new Material("14", "", 0, 400, "ДВП 4 мм", 1, "лист"),
            new Material("15", "", 0, 800, "ЛДВП 4 мм", 1, "лист"),
            new Material("16", "", 0, 1000, "Карниз", 2, "шт."),
            new Material("17", "", 0, 200, "Цоколь", 2, "п.м."),
            new Material("18", "", 0, 800, "Столешница ЛДСП 28 мм", 2, "п.м."),
            new Material("19", "", 0, 1000, "Столешница ЛДСП 38 мм", 2, "п.м."),
            new Material("20", "", 0, 1400, "ЛДСП Вишня Оксфорд 2750х1830х16мм", 1, "лист"),
            new Material("21", "", 0, 1400, "ЛДСП Вишня Оксфорд 2750х1830х16мм", 1, "лист"),
            new Material("22", "", 0, 1400, "ЛДСП Вишня Оксфорд 2750х1830х16мм", 1, "лист"),
            new Material("23", "", 0, 1400, "ЛДСП Вишня Оксфорд 2750х1830х16мм", 1, "лист"),
            new Material("24", "", 0, 1400, "ЛДСП Вишня Оксфорд 2750х1830х16мм", 1, "лист"),
            new Material("25", "", 0, 1400, "ЛДСП Вишня Оксфорд 2750х1830х16мм", 1, "лист"),
            new Material("26", "", 0, 1400, "ЛДСП Вишня Оксфорд 2750х1830х16мм", 1, "лист"),
            new Material("27", "", 0, 1400, "ЛДСП Вишня Оксфорд 2750х1830х16мм", 1, "лист"),
            new Material("28", "", 0, 1400, "ЛДСП Вишня Оксфорд 2750х1830х16мм", 1, "лист"),

    };

}