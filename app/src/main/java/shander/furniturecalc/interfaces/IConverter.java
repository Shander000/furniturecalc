package shander.furniturecalc.interfaces;

public interface IConverter<S, D> {

    D convert(S src);

}
