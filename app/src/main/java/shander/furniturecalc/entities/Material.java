package shander.furniturecalc.entities;

import java.io.Serializable;
import java.util.Objects;

public class Material implements Serializable {

    private String article = "";
    private String comment = "";
    private int id;
    private double price = 0;
    private String title = "";
    private int type = 0;
    private String unit = "";

    public Material() {
    }

    public Material(String article, String comment, int id, double price, String title, int type, String unit) {
        this.article = article;
        this.comment = comment;
        this.id = id;
        this.price = price;
        this.title = title;
        this.type = type;
        this.unit = unit;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Material material = (Material) o;
        return getId() == material.getId() &&
                Double.compare(material.getPrice(), getPrice()) == 0 &&
                getType() == material.getType() &&
                Objects.equals(getArticle(), material.getArticle()) &&
                Objects.equals(getTitle(), material.getTitle()) &&
                Objects.equals(getUnit(), material.getUnit());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getArticle(), getId(), getPrice(), getTitle(), getType(), getUnit());
    }
}
