package shander.furniturecalc.entities;

import android.util.Log;
import android.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;

public class NAAdapterEntity {

    private LinkedHashMap<NotAvailableMaterial, Project> entries = new LinkedHashMap<>();
    private LinkedHashMap<String, NotAvailableMaterial> groups = new LinkedHashMap<>();
    private LinkedHashMap<NotAvailableMaterial, LinkedHashMap<Project, Double>> readyEntries = new LinkedHashMap<>();

    public NAAdapterEntity(ArrayList<NotAvailableMaterial> materials, ArrayList<Project> projects) {
        for (int i = 0; i < materials.size(); i++) {
            entries.put(materials.get(i), projects.get(i));
        }
        for (NotAvailableMaterial material : entries.keySet()) {
            NotAvailableMaterial groupMat = new NotAvailableMaterial(material.getId(), material.getMaterial(), material.getCount(), material.getProjectId());
            if (!groups.containsKey(groupMat.getMaterial().getTitle())) {
                groups.put(groupMat.getMaterial().getTitle(), groupMat);
            } else {
                groups.get(groupMat.getMaterial().getTitle()).setCount(groups.get(groupMat.getMaterial().getTitle()).getCount() + groupMat.getCount());
            }
        }
        for (NotAvailableMaterial material3 : groups.values()) {
            LinkedHashMap<Project, Double> groupEntries = new LinkedHashMap<>();
            for (NotAvailableMaterial material1 : entries.keySet()) {
                if (material1.getMaterial().getId() == material3.getMaterial().getId() &&
                        material1.getMaterial().getType() == material3.getMaterial().getType()) {
                    groupEntries.put(entries.get(material1), material1.getCount());
                    Log.wtf("group array FILL", entries.get(material1).getmName() + "|" + material1.getCount());
                }
            }
            readyEntries.put(material3, groupEntries);
        }
    }

    public LinkedHashMap<NotAvailableMaterial, LinkedHashMap<Project, Double>> getReadyEntries() {
        return readyEntries;
    }

    public LinkedHashMap<NotAvailableMaterial, Project> getEntries() {
        return entries;
    }

    LinkedHashMap<String, NotAvailableMaterial> getGroups () {
        return groups;
    }

    public int getGroupsCount() {
        return groups.size();
    }

    public int getChildCount(int groupPos) {
        return readyEntries.get(new ArrayList<>(readyEntries.keySet()).get(groupPos)).size();
    }

    public Pair<String, NotAvailableMaterial> getSingleGroup (int pos) {
        String first = new ArrayList<>(groups.keySet()).get(pos);
        NotAvailableMaterial second = groups.get(first);
        return new Pair<>(first, second);
    }

    public LinkedHashMap<Project, Double> getChildsByGroup (int pos) {
        NotAvailableMaterial groupMaterial = new ArrayList<>(readyEntries.keySet()).get(pos);
        return readyEntries.get(groupMaterial);
    }

    public Pair<Project, Double> getSingleChild (int parentPos, int childPos) {
        NotAvailableMaterial groupMaterial = new ArrayList<>(readyEntries.keySet()).get(parentPos);
        LinkedHashMap<Project, Double> groupEntries = readyEntries.get(groupMaterial);
        Project targetProject = new ArrayList<>(groupEntries.keySet()).get(childPos);
        Double targetCount = groupEntries.get(targetProject);
        return new Pair<>(targetProject, targetCount);
    }

}
