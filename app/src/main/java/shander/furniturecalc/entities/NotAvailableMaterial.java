package shander.furniturecalc.entities;

import java.io.Serializable;
import java.util.Objects;

public class NotAvailableMaterial implements Serializable {

    private int id;
    private Material material;
    private double count;
    private int projectId;

    public NotAvailableMaterial(int id, Material material, double count, int projectId) {
        this.id = id;
        this.material = material;
        this.count = count;
        this.projectId = projectId;
    }

    public NotAvailableMaterial() {
    }

    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        NotAvailableMaterial material1 = (NotAvailableMaterial) o;
//        return getId() == material1.getId() &&
//                Double.compare(material1.getCount(), getCount()) == 0 &&
//                getProjectId() == material1.getProjectId() &&
//                Objects.equals(getMaterial(), material1.getMaterial());
//    }
//
//    @Override
//    public int hashCode() {
//
//        return Objects.hash(getId(), getMaterial(), getCount(), getProjectId());
//    }
}
