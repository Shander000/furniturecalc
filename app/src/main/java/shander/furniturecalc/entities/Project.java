package shander.furniturecalc.entities;

import android.content.ContentValues;
import android.database.Cursor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;


public class Project implements Serializable {

    private String mName;
    private String mFIO;
    private Double mCost;
    private String mAddress;
    private String mPhone;
    private String mNote;
    private int id;
    private ArrayList<Calculation> calculations = new ArrayList<>();

    public Project(String mName, String mFIO, Double mCost, String mAddress, String mPhone, String mNote, int id) {
        this.mName = mName;
        this.mFIO = mFIO;
        this.mCost = mCost;
        this.mAddress = mAddress;
        this.mPhone = mPhone;
        this.mNote = mNote;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Project() {
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmFIO() {
        return mFIO;
    }

    public void setmFIO(String mFIO) {
        this.mFIO = mFIO;
    }

    public Double getmCost() {
        return mCost;
    }

    public void setmCost(Double mCost) {
        this.mCost = mCost;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getmPhone() {
        return mPhone;
    }

    public void setmPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    public String getmNote() {
        return mNote;
    }

    public void setmNote(String mNote) {
        this.mNote = mNote;
    }

    public ArrayList<Calculation> getCalculations() {
        return calculations;
    }

    public void setCalculations(ArrayList<Calculation> calculations) {
        this.calculations = calculations;
        double cost = 0.0;
        if (calculations.size() != 0) {
            for (Calculation calculation : calculations) {
                cost += calculation.getFullPrice();
            }
        }
        this.mCost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return getId() == project.getId() &&
                Objects.equals(getmName(), project.getmName()) &&
                Objects.equals(getmFIO(), project.getmFIO()) &&
                Objects.equals(getmCost(), project.getmCost()) &&
                Objects.equals(getmAddress(), project.getmAddress()) &&
                Objects.equals(getmPhone(), project.getmPhone()) &&
                Objects.equals(getmNote(), project.getmNote());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getmName(), getmFIO(), getmCost(), getmAddress(), getmPhone(), getmNote(), getId());
    }
}
