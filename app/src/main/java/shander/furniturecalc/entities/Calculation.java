package shander.furniturecalc.entities;

import java.io.Serializable;
import java.util.Objects;

public class Calculation implements Serializable {

    private double amount;
    private String comment;
    private double fullPrice;
    private int projectId;
    private boolean isNds;
    private boolean isPercent;
    private int nds;
    private double percent;
    private String title;
    private int uId;
    private boolean isMaterialAvailable = false;
    private Material material;

    public Calculation(double amount, String comment, double fullPrice, int projectId, boolean isNds,
                       boolean isPercent, int nds, double percent, String title, int uId,
                       boolean isMaterialAvailable, Material material) {
        this.amount = amount;
        this.comment = comment;
        this.fullPrice = fullPrice;
        this.projectId = projectId;
        this.isNds = isNds;
        this.isPercent = isPercent;
        this.nds = nds;
        this.percent = percent;
        this.title = title;
        this.uId = uId;
        this.isMaterialAvailable = isMaterialAvailable;
        this.material = material;
    }

    public boolean isMaterialAvailable() {
        return isMaterialAvailable;
    }

    public void setMaterialAvailable(boolean materialAvailable) {
        isMaterialAvailable = materialAvailable;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public Calculation() {
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public double getFullPrice() {
        return fullPrice;
    }

    public void setFullPrice(double fullPrice) {
        this.fullPrice = fullPrice;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int id) {
        this.projectId = id;
    }

    public boolean getIsNds() {
        return isNds;
    }

    public void setIsNds(boolean isNds) {
        this.isNds = isNds;
    }

    public boolean getIsPercent() {
        return isPercent;
    }

    public void setIsPercent(boolean isPercent) {
        this.isPercent = isPercent;
    }

    public int getNds() {
        return nds;
    }

    public void setNds(int nds) {
        this.nds = nds;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getuId() {
        return uId;
    }

    public void setuId(int uId) {
        this.uId = uId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Calculation that = (Calculation) o;
        return getProjectId() == that.getProjectId() &&
                getNds() == that.getNds() &&
                getPercent() == that.getPercent() &&
                getNds() == that.getNds() &&
                Double.compare(that.getPercent(), getPercent()) == 0 &&
                getuId() == that.getuId() &&
                Objects.equals(getTitle(), that.getTitle()) &&
                Objects.equals(getMaterial(), that.getMaterial());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getProjectId(), getNds(), getPercent(), getNds(), getPercent(), getTitle(), getuId(), getMaterial());
    }
}
