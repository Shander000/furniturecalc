package shander.furniturecalc.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class BaseCatalogue implements Serializable {

    public BaseCatalogue() {
    }

    private String name;
    private int catId;
    private ArrayList<Material> materials = new ArrayList<>();

    public BaseCatalogue(String name, int catId) {
        this.name = name;
        this.catId = catId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public ArrayList<Material> getMaterials() {
        return materials;
    }

    public void setMaterials(ArrayList<Material> materials) {
        this.materials = materials;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseCatalogue that = (BaseCatalogue) o;
        return getCatId() == that.getCatId() &&
                Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {

        int result = Objects.hash(getName(), getCatId());
        result = 31 * result;
        return result;
    }
}
