package shander.furniturecalc;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.IBinder;
import android.preference.PreferenceManager;
//import android.support.multidex.MultiDex;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.android.vending.billing.IInAppBillingService;

import java.lang.reflect.Field;

import shander.furniturecalc.db.DBHelper;
import shander.furniturecalc.ui.LaunchActivity;
import shander.furniturecalc.ui.SettingsActivity;

public class CalcApp extends MultiDexApplication {

    private static CalcApp mApplicationInstance = null;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private DBHelper dbHelper;

    public static CalcApp getApplication() {
        return mApplicationInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();
        if (!preferences.getString("fontSize", "").equals("")) {
            overrideFontSize(preferences.getString("fontSize", ""));
        }
        mApplicationInstance = this;
        dbHelper = new DBHelper(this, 1);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public DBHelper getDbHelper() {
        return dbHelper;
    }

    public String getBase64Key() {
        return getResources().getString(R.string.base_64_app_key_part_one) + getResources().getString(R.string.base_64_app_key_part_four)
                + getResources().getString(R.string.base_64_app_key_part_nine) + getResources().getString(R.string.base_64_app_key_part_two)
                + getResources().getString(R.string.base_64_app_key_part_seven) + getResources().getString(R.string.base_64_app_key_part_five)
                + getResources().getString(R.string.base_64_app_key_part_ten) + getResources().getString(R.string.base_64_app_key_part_three)
                + getResources().getString(R.string.base_64_app_key_part_eight) + getResources().getString(R.string.base_64_app_key_part_six);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public void overrideFont(Context context, String customFontFileNameInAssets) {
        try {
            final Typeface customFontTypeface = Typeface.createFromAsset(context.getAssets(), customFontFileNameInAssets + ".ttf");
            final Field defaultFontTypefaceField = Typeface.class.getDeclaredField("SERIF");
            defaultFontTypefaceField.setAccessible(true);
            defaultFontTypefaceField.set(null, customFontTypeface);
            Intent intent = new Intent(this, SettingsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (Exception e) {
            Log.d("Fonts Error", "Can not set custom font " + customFontFileNameInAssets + " instead of " + "SERIF");
        }
    }

    public void overrideFontSize (String fontSize) {
        switch (fontSize){
            case "normal":
                getApplicationContext().setTheme(R.style.AppTheme);
                break;
            case "large":
                getApplicationContext().setTheme(R.style.AppTheme_LargeFont);
                break;
            case "small":
                getApplicationContext().setTheme(R.style.AppTheme_SmallFont);
        }
        Intent intent = new Intent(this, SettingsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    public SharedPreferences getPrefs() {
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        return preferences;
    }

    public SharedPreferences.Editor getEditor() {
        return editor;
    }

}
