package shander.furniturecalc.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import shander.furniturecalc.CalcApp;
import shander.furniturecalc.R;
import shander.furniturecalc.entities.BaseCatalogue;
import shander.furniturecalc.entities.Calculation;
import shander.furniturecalc.utils.JSONHelper;

public class SummaryAdapter extends RecyclerView.Adapter<SummaryAdapter.CategoryHolder>  {

    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private Map<Integer, Pair<String, Double>> calculations = new HashMap<>();
    private Map<Integer, Pair<String, Double>> calculationsTemp = new HashMap<>();
    private ArrayList<Pair<String, Double>> categoriesList = new ArrayList<>();

    public SummaryAdapter(Context mContext) {
        mLayoutInflater = LayoutInflater.from(mContext);
        this.mContext = mContext;
        ArrayList<BaseCatalogue> catalogues = CalcApp.getApplication().getDbHelper().getAllCategories();
        for (BaseCatalogue catalogue : catalogues) {
            calculationsTemp.put(catalogue.getCatId(), new Pair<>(catalogue.getName(), 0.0));
        }
    }

    public void setItems (ArrayList<Calculation> items) {
        if (items != null) {

            for (Calculation calculation : items) {
                Pair<String, Double> temp = calculationsTemp.get(calculation.getMaterial().getType());
                if (temp == null) {
                    calculations.put(calculation.getMaterial().getType(), new Pair<>(mContext.getResources().getString(R.string.deleted_category), 0.0));
                } else if (!calculations.containsKey(calculation.getMaterial().getType())) {
                    calculations.put(calculation.getMaterial().getType(), temp);
                }
                Pair<String, Double> newTemp = calculations.get(calculation.getMaterial().getType());
                newTemp = new Pair<>(newTemp.first, newTemp.second + calculation.getMaterial().getPrice()*calculation.getAmount());
                calculations.put(calculation.getMaterial().getType(), newTemp);
                Log.d("CATS", "INC" + calculations.size());
            }
            categoriesList = new ArrayList<>(calculations.values());
            Log.d("CATS_LIST", "INC" + categoriesList.size());
        }
        notifyDataSetChanged();
    }

    @Override
    public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CategoryHolder(mLayoutInflater.inflate(R.layout.item_calculation, parent, false));
    }

    @Override
    public void onBindViewHolder(CategoryHolder holder, int position) {
        holder.setItem(new ArrayList<>(calculations.values()).get(position), position);
    }

    @Override
    public int getItemCount() {
        return calculations.size();
    }

    class CategoryHolder extends RecyclerView.ViewHolder {

        private TextView itemName;
        private TextView itemPrice;
        private CheckBox deleteCheck;

        public CategoryHolder(View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.item_calc_name);
            itemPrice = itemView.findViewById(R.id.item_calc_price);
            deleteCheck = itemView.findViewById(R.id.delete_check);
            deleteCheck.setVisibility(View.GONE);

        }

        void setItem(Pair<String, Double> item, int position) {
            itemName.setText(item.first);
            itemPrice.setText(String.valueOf(String.format(Locale.getDefault(),"%.2f",item.second)));
        }
    }


}
