package shander.furniturecalc.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import shander.furniturecalc.R;
import shander.furniturecalc.entities.BaseCatalogue;
import shander.furniturecalc.entities.Material;

public class FurnitureAdapter extends RecyclerView.Adapter<FurnitureAdapter.FurnitureItemHolder> {

    private BaseCatalogue catalogue;
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private OnItemClickListener mItemClickListener;
    private boolean mIsCheckBoxEnabled = false;
    boolean[] checked;

    public FurnitureAdapter(Context mContext) {
        this.mContext = mContext;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    public void setItems (BaseCatalogue items) {
        catalogue = items;
        checked = new boolean[catalogue.getMaterials().size()];
        notifyDataSetChanged();
    }

    public boolean[] getChecked() {
        return checked;
    }

    public void setCheckBoxEnabled(boolean enabled){
        mIsCheckBoxEnabled = enabled;
        notifyDataSetChanged();
    }

    public boolean getCheckBoxEnabled () {
        return mIsCheckBoxEnabled;
    }

    @Override
    public FurnitureItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FurnitureItemHolder(mLayoutInflater.inflate(R.layout.item_base, parent, false));
    }

    @Override
    public void onBindViewHolder(FurnitureItemHolder holder, int position) {
        Material material = catalogue.getMaterials().get(position);
        holder.setItem(material, position);
        holder.deleteCheck.setChecked(checked[position]);
        holder.deleteCheck.setVisibility(mIsCheckBoxEnabled ? View.VISIBLE :View.GONE);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Material material);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public int getItemCount() {
        return catalogue.getMaterials().size();
    }

    class FurnitureItemHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private TextView price;
        private CheckBox deleteCheck;

        public FurnitureItemHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.item_base_text);
            deleteCheck = itemView.findViewById(R.id.delete_check);deleteCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    checked[getLayoutPosition()] = b;
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (mItemClickListener != null) {
                        mItemClickListener.onItemClick(v, catalogue.getMaterials().get(getLayoutPosition()));
                    }
                }
            });
            price = itemView.findViewById(R.id.item_base_price);
        }

        void setItem(Material item, int position) {
            name.setText(item.getTitle());
            price.setText(String.valueOf(item.getPrice()));
        }
    }
}
