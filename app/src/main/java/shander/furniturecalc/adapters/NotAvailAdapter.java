package shander.furniturecalc.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import shander.furniturecalc.CalcApp;
import shander.furniturecalc.R;
import shander.furniturecalc.db.DBHelper;
import shander.furniturecalc.entities.NAAdapterEntity;
import shander.furniturecalc.entities.NotAvailableMaterial;
import shander.furniturecalc.entities.Project;

public class NotAvailAdapter extends RecyclerView.Adapter<NotAvailAdapter.ItemHolder>{

    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private NAAdapterEntity entity;
    private DBHelper dbHelper;
    private boolean mIsDeleteEnabled = false;
    Integer[] checked;
    private NAListener listener;

    public NotAvailAdapter(LayoutInflater mLayoutInflater, Context mContext, NAListener listener) {
        this.mLayoutInflater = mLayoutInflater;
        this.mContext = mContext;
        dbHelper = CalcApp.getApplication().getDbHelper();
        this.listener = listener;
        updateData();
    }

    public Integer[] getChecked() {
        return checked;
    }

    public void updateData() {
        Log.wtf("ADAPTER UPDATE ", "+");
        ArrayList<NotAvailableMaterial> materials = dbHelper.getAllNotAvailable();
        ArrayList<Project> projects = dbHelper.getProjectsWithNA();
        entity = new NAAdapterEntity(materials, projects);
        checked = new Integer[entity.getGroupsCount()];
        double sum = 0;
        for (int i = 0; i < entity.getGroupsCount(); i++) {
            sum += entity.getSingleGroup(i).second.getCount() * entity.getSingleGroup(i).second.getMaterial().getPrice();
        }
        listener.onItemsLoaded(sum);
        notifyDataSetChanged();
    }

    public void setmIsDeleteEnabled(boolean mIsDeleteEnabled) {
        this.mIsDeleteEnabled = mIsDeleteEnabled;
        notifyDataSetChanged();
    }

    public boolean ismIsDeleteEnabled() {
        return mIsDeleteEnabled;
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemHolder(mLayoutInflater.inflate(R.layout.item_na_group, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
        holder.setItem(entity.getSingleGroup(position).second);
        if (checked[position] != null) {
            holder.deleteCheck.setChecked(checked[position] != 0);
        }
        holder.deleteCheck.setVisibility(mIsDeleteEnabled ? View.VISIBLE :View.GONE);
    }

    @Override
    public int getItemCount() {
        return entity.getGroupsCount();
    }

    class ItemHolder extends RecyclerView.ViewHolder{

        private TextView name;
        private TextView count;
        private CheckBox deleteCheck;

        public ItemHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.na_group_name);
            count = itemView.findViewById(R.id.na_group_count);
            deleteCheck = itemView.findViewById(R.id.na_group_check);
            deleteCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    checked[getLayoutPosition()] = isChecked ? entity.getSingleGroup(getLayoutPosition()).second.getMaterial().getId() : 0;
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onNAClick(entity.getChildsByGroup(getLayoutPosition()), entity.getSingleGroup(getLayoutPosition()).second.getMaterial().getTitle(),
                                entity.getSingleGroup(getLayoutPosition()).second.getMaterial().getUnit());
                    }
                }
            });
        }

        void setItem (NotAvailableMaterial material) {
            name.setText(material.getMaterial().getTitle());
            count.setText(String.format("%.2f",(material.getCount()*material.getMaterial().getPrice())));
        }
    }

    public interface NAListener {
        void onItemsLoaded(double sum);
        void onNAClick(LinkedHashMap<Project, Double> projects, String title, String unit);
    }
}
