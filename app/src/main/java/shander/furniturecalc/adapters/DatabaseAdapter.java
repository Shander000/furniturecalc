package shander.furniturecalc.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import shander.furniturecalc.R;
import shander.furniturecalc.entities.BaseCatalogue;
import shander.furniturecalc.entities.Calculation;

public class DatabaseAdapter extends RecyclerView.Adapter<DatabaseAdapter.ItemHolder> {

    private ArrayList<BaseCatalogue> catalogues;
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private OnItemClickListener mItemClickListener;
    private boolean mIsCheckBoxEnabled = false;
    boolean[] checked;

    public DatabaseAdapter(Context mContext) {
        this.mContext = mContext;
        mLayoutInflater = LayoutInflater.from(mContext);
        catalogues = new ArrayList<>();
    }

    public void setItems (ArrayList<BaseCatalogue> items) {
        catalogues = items;
        checked = new boolean[catalogues.size()];
        notifyDataSetChanged();
    }

    public void setCheckBoxEnabled(boolean enabled){
        mIsCheckBoxEnabled = enabled;
        notifyDataSetChanged();
    }

    public boolean getCheckBoxEnabled () {
        return mIsCheckBoxEnabled;
    }

    public void addItem(BaseCatalogue cat) {
        catalogues.add(cat);
        checked = new boolean[catalogues.size()];
        notifyDataSetChanged();
    }

    public boolean[] getChecked() {
        return checked;
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DatabaseAdapter.ItemHolder(mLayoutInflater.inflate(R.layout.item_project, parent, false));
    }

    @Override
    public void onBindViewHolder(ItemHolder holder, int position) {
        BaseCatalogue item = catalogues.get(position);
        holder.setItem(item, position);
        holder.deleteCheck.setChecked(checked[position]);
        holder.deleteCheck.setVisibility(mIsCheckBoxEnabled ? View.VISIBLE :View.GONE);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, BaseCatalogue catalogue);
    }

    @Override
    public int getItemCount() {
        return catalogues.size();
    }

    class ItemHolder extends RecyclerView.ViewHolder{

        private TextView name;
        private CheckBox deleteCheck;

        public ItemHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.title);
            deleteCheck = itemView.findViewById(R.id.delete_check);
            deleteCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    checked[getLayoutPosition()] = b;
                }
            });
            itemView.findViewById(R.id.name).setVisibility(View.GONE);
            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (mItemClickListener != null) {
                        mItemClickListener.onItemClick(v, catalogues.get(getLayoutPosition()));
                    }
                }
            });
        }

        void setItem(BaseCatalogue item, int position) {
            name.setText(item.getName());
        }
    }
}
