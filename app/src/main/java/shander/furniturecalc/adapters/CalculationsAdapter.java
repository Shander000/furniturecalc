package shander.furniturecalc.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import shander.furniturecalc.R;
import shander.furniturecalc.entities.Calculation;


public class CalculationsAdapter extends RecyclerView.Adapter<CalculationsAdapter.ItemHolder> {

    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private ArrayList<Calculation> calculations = new ArrayList<>();
    private CalcListener listener;
    private boolean mIsCheckBoxEnabled = false;
    boolean[] checked;

    public CalculationsAdapter(Context mContext) {
        this.mContext = mContext;
        mLayoutInflater = LayoutInflater.from(mContext);
        calculations = new ArrayList<>();
    }

    public void setCheckBoxEnabled(boolean enabled){
        mIsCheckBoxEnabled = enabled;
        notifyDataSetChanged();
    }
    public boolean getCheckBoxEnabled () {
        return mIsCheckBoxEnabled;
    }

    public void setItems (ArrayList<Calculation> items) {
        if (items != null) {
            calculations = items;
        }
        checked = new boolean[calculations.size()];
        notifyDataSetChanged();
    }

    public boolean[] getChecked() {
        return checked;
    }

    public ArrayList<Calculation> getCalculations() {
        return calculations;
    }

    public void setListener (CalcListener listener) {
        this.listener = listener;
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemHolder(mLayoutInflater.inflate(R.layout.item_calculation, parent, false));
    }

    @Override
    public void onBindViewHolder(ItemHolder holder, int position) {
        Calculation item = calculations.get(position);
        holder.setItem(item, position);
        holder.deleteCheck.setChecked(checked[position]);
        holder.deleteCheck.setVisibility(mIsCheckBoxEnabled ? View.VISIBLE :View.GONE);
    }

    @Override
    public int getItemCount() {
        return calculations.size();
    }

    class ItemHolder extends RecyclerView.ViewHolder {

        private TextView itemName;
        private TextView itemPrice;
        private CheckBox deleteCheck;

        ItemHolder(View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.item_calc_name);
            itemPrice = itemView.findViewById(R.id.item_calc_price);
            deleteCheck = itemView.findViewById(R.id.delete_check);
            deleteCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    checked[getLayoutPosition()] = b;
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onCalcSelected(calculations.get(getLayoutPosition()));
                    }
                }
            });
        }

        void setItem(Calculation item, int position) {
            itemName.setText(item.getTitle());
            itemPrice.setText(String.valueOf(item.getFullPrice()));
        }
    }

    public interface CalcListener {
        void onCalcSelected (Calculation calculation);
    }
}
