package shander.furniturecalc.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import shander.furniturecalc.R;
import shander.furniturecalc.entities.Project;

public class ProjectsAdapter extends  RecyclerView.Adapter<ProjectsAdapter.ItemHolder>{

    private ArrayList<Project> projects;
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private Listener mListener;
    private boolean mIsCheckBoxEnabled = false;
    boolean[] checked;

    public ProjectsAdapter(Context mContext) {
        this.mContext = mContext;
        mLayoutInflater = LayoutInflater.from(mContext);
        projects = new ArrayList<>();
    }

    public void setCheckBoxEnabled(boolean enabled){
        mIsCheckBoxEnabled = enabled;
        notifyDataSetChanged();
    }

    public void setItems(ArrayList<Project> projects){
        this.projects = projects;
        checked = new boolean[projects.size()];
        notifyDataSetChanged();
    }

    public boolean[] getChecked() {
        return checked;
    }

    public boolean getCheckBoxEnabled () {
        return mIsCheckBoxEnabled;
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemHolder(mLayoutInflater.inflate(R.layout.item_project, parent, false));
    }

    @Override
    public void onBindViewHolder(ItemHolder holder, int position) {
        holder.setItem(projects.get(position));
        holder.deleteCheck.setChecked(checked[position]);
        holder.deleteCheck.setVisibility(mIsCheckBoxEnabled ? View.VISIBLE :View.GONE);
    }

    public void setOnItemClickListener(Listener listener) {
        this.mListener = listener;
    }

    @Override
    public int getItemCount() {
        return projects.size();
    }

    public interface Listener {
        void onItemClick(View view, Project project);
    }

    class ItemHolder extends RecyclerView.ViewHolder{

        private TextView name;
        private TextView title;
        private CheckBox deleteCheck;

        public ItemHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            title = itemView.findViewById(R.id.title);
            deleteCheck = itemView.findViewById(R.id.delete_check);
            deleteCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    checked[getLayoutPosition()] = b;
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        mListener.onItemClick(view, projects.get(getLayoutPosition()));
                    }
                }
            });
        }

        void setItem(Project project) {
            title.setText(project.getmName());
            name.setText(mContext.getResources().getString(R.string.rub, String.format("%.2f", project.getmCost())));
        }
    }
}