package shander.furniturecalc.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import shander.furniturecalc.CalcApp;
import shander.furniturecalc.R;
import shander.furniturecalc.dialogs.MessageDialog;
import shander.furniturecalc.interfaces.IContract;
import shander.furniturecalc.util.IabHelper;
import shander.furniturecalc.util.IabResult;
import shander.furniturecalc.util.Purchase;
import shander.furniturecalc.utils.LocalAppCompatActivity;

public class BillingActivity extends LocalAppCompatActivity implements MessageDialog.Listener{

    IabHelper mHelper;
    Purchase purchase = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing);

        configureToolbar();
        setCustomTitle(getResources().getString(R.string.pay));

        ImageButton temp = (ImageButton) findViewById(R.id.title_btn_edit);
        temp.setVisibility(View.GONE);
        Button billing = (Button) findViewById(R.id.button);
        billing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mHelper != null) {
                    try {
                        mHelper.launchPurchaseFlow(BillingActivity.this, IContract.PREMIUM_ZERO, 10001,
                                mPurchaseFinishedListener, "");
                    } catch (IabHelper.IabAsyncInProgressException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        mHelper = new IabHelper(this, CalcApp.getApplication().getBase64Key());
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh no, there was a problem.
                    Log.wtf("HELPER SETUP", "Problem setting up In-app Billing: " + result);
                }
                // Hooray, IAB is fully set up!
            }
        });
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, final Purchase purchase)
        {
            if (result.isFailure()) {
                Log.wtf("BUY RESULT", "Error purchasing: " + result);
            }
            else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        CalcApp.getApplication().getEditor().putBoolean(IContract.PREMIUM_ZERO, purchase.getSku().equals(IContract.PREMIUM_ZERO) &&
                                (purchase.getPurchaseState() == 0)).apply();
                        Log.wtf("SETTINGS", CalcApp.getApplication().getPrefs().getBoolean(IContract.PREMIUM_ZERO, false) + "|");
                        goToMain();
                    }
                });
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        goToMain();
    }

    @Override
    public void onMessageDialogDismissed(int messageId) {
        goToMain();
    }

    private void goToMain() {
        finish();
        Intent intent = new Intent(BillingActivity.this, LaunchActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) {
            try {
                mHelper.dispose();
            } catch (IabHelper.IabAsyncInProgressException e) {
                e.printStackTrace();
            }
        }
        mHelper = null;
    }
}
