package shander.furniturecalc.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import shander.furniturecalc.CalcApp;
import shander.furniturecalc.R;
import shander.furniturecalc.adapters.CalculationsAdapter;
import shander.furniturecalc.adapters.SummaryAdapter;
import shander.furniturecalc.entities.Calculation;
import shander.furniturecalc.entities.Project;
import shander.furniturecalc.interfaces.IContract;
import shander.furniturecalc.utils.LocalAppCompatActivity;

public class SummaryActivity extends LocalAppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.recycler_view)
    RecyclerView calcList;
    @BindView(R.id.empty_list)
    TextView emptyListText;
    @BindView(R.id.nds_sum)
    TextView ndsSummary;
    @BindView(R.id.coeff_sum)
    TextView coeffSum;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.sum)
    TextView allSum;

    private Project project;
    private Double coeff = 0.0;
    private Double nds = 0.0;
    private Double sum = 0.0;
    private SummaryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);
        ButterKnife.bind(this);
        configureToolbar();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        if (CalcApp.getApplication().getPrefs().getBoolean(IContract.PREMIUM_ZERO, false)) {
            navigationView.getMenu().getItem(7).setVisible(false);
        } else {
            navigationView.getMenu().getItem(7).setVisible(true);
        }

        project = (Project) getIntent().getSerializableExtra(IContract.PROJECT);
        if (project == null) {
            finish();
        }
        ImageButton btnEdit = toolbar.findViewById(R.id.title_btn_edit);
        btnEdit.setVisibility(View.GONE);

        adapter = new SummaryAdapter(this);
        calcList.setLayoutManager(new LinearLayoutManager(this));
        calcList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        calcList.setAdapter(adapter);
        if (project != null) {
            if (project.getCalculations() != null && project.getCalculations().size() != 0) {
                adapter.setItems(project.getCalculations());
                for (Calculation calculation : project.getCalculations()) {
                    if (calculation.getIsNds()) {
                        nds = nds + calculation.getNds() * calculation.getMaterial().getPrice() * calculation.getAmount() / 100;
                    }
                    if (calculation.getIsPercent()) {
                        coeff = coeff + calculation.getPercent() * calculation.getMaterial().getPrice() * calculation.getAmount() / 100;
                    }
                    sum = sum + calculation.getFullPrice();
                }
                ndsSummary.setText(getResources().getString(R.string.rub, String.valueOf(nds)));
                coeffSum.setText(getResources().getString(R.string.rub, String.valueOf(coeff)));
                allSum.setText(getResources().getString(R.string.rub, String.format("%.2f", sum)));
            } else {
                Intent intent0 = new Intent(SummaryActivity.this, ProjectsActivity.class);
                startActivity(intent0);
                finish();
            }
        } else {
            Intent intent0 = new Intent(SummaryActivity.this, ProjectsActivity.class);
            startActivity(intent0);
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_main:
                Intent intent = new Intent(SummaryActivity.this, LaunchActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.nav_na:
                Intent intent1 = new Intent(SummaryActivity.this, NotAvailableActivity.class);
                startActivity(intent1);
                finish();
                break;
            case R.id.nav_projects:
                Intent intent0 = new Intent(SummaryActivity.this, ProjectsActivity.class);
                startActivity(intent0);
                finish();
                break;
            case R.id.nav_base:
                Intent intent2 = new Intent(SummaryActivity.this, BaseSelectActivity.class);
                startActivity(intent2);
                finish();
                break;
            case R.id.nav_count:
                Intent intent5 = new Intent(SummaryActivity.this, ProjectAddActivity.class);
                startActivity(intent5);
                finish();
                break;
            case R.id.nav_settings:
                Intent intent6 = new Intent(SummaryActivity.this, SettingsActivity.class);
                startActivity(intent6);
                finish();

                break;
            case R.id.nav_about:
                Intent intent4 = new Intent(SummaryActivity.this, AboutActivity.class);
                startActivity(intent4);
                finish();
                break;
            case R.id.nav_pay:
                Intent intent3 = new Intent(SummaryActivity.this, BillingActivity.class);
                startActivity(intent3);
                finish();
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }
}
