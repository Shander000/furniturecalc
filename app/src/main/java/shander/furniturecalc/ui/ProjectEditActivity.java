package shander.furniturecalc.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import shander.furniturecalc.CalcApp;
import shander.furniturecalc.R;
import shander.furniturecalc.adapters.CalculationsAdapter;
import shander.furniturecalc.db.DBHelper;
import shander.furniturecalc.entities.Calculation;
import shander.furniturecalc.entities.Project;
import shander.furniturecalc.interfaces.IContract;
import shander.furniturecalc.utils.JSONHelper;
import shander.furniturecalc.utils.LocalAppCompatActivity;

public class ProjectEditActivity extends LocalAppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private Project project;
    private CalculationsAdapter adapter;
    private int id;
    private Project tempId = null;
    private Calculation newCalculation;
    private ImageButton btnEdit;
    private DBHelper dbHelper;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.sum)
    TextView tvSum;
    @BindView(R.id.add_calculation)
    ImageButton addCalc;
    @BindView(R.id.summary_lay)
    LinearLayout summaryLay;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @OnClick(R.id.summary_lay)
    void click() {
        Intent intent = new Intent(ProjectEditActivity.this, SummaryActivity.class);
        intent.putExtra(IContract.PROJECT, project);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_edit);

        configureToolbar();
        ButterKnife.bind(this);

        dbHelper = CalcApp.getApplication().getDbHelper();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        final View.OnClickListener addListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    Intent intent = new Intent(ProjectEditActivity.this, BaseSelectActivity.class);
                    intent.putExtra(IContract.ID, project.getId());
                    intent.putExtra(IContract.FROM_PROJECT, true);
//            dbHelper.saveProject(project);
                    startActivity(intent);
                    finish();
                }
            }
        } ;

        final View.OnClickListener deleteListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    boolean[] toDelete = adapter.getChecked();
                    ArrayList<Calculation> calculations = project.getCalculations();
                    for (int i = toDelete.length - 1; i >= 0; i--) {
                        if (toDelete[i]) {
                            dbHelper.deleteCalculation(calculations.get(i).getuId());
                            calculations.remove(i);
                        }
                    }
                    adapter.setItems(calculations);
                    project.setCalculations(calculations);
//            dbHelper.saveProject(project);
                    btnEdit.performClick();
                    recalculatePrice();
                    if (calculations.size() == 0) {
                        addCalc.setImageResource(R.drawable.ic_add);
                        addCalc.setOnClickListener(addListener);
                        addCalc.setTranslationX(0);
                        addCalc.setTranslationY(0);
                    }
                }
            }
        } ;

        btnEdit = toolbar.findViewById(R.id.title_btn_edit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (adapter.getItemCount() != 0) {
                    adapter.setCheckBoxEnabled(!adapter.getCheckBoxEnabled());
                    addCalc.setImageResource(adapter.getCheckBoxEnabled() ? android.support.design.R.drawable.abc_ic_clear_material : R.drawable.ic_add);
                    addCalc.setOnClickListener(adapter.getCheckBoxEnabled() ? deleteListener : addListener);
                }
            }
        });

        navigationView.setNavigationItemSelectedListener(this);
        if (CalcApp.getApplication().getPrefs().getBoolean(IContract.PREMIUM_ZERO, false)) {
            navigationView.getMenu().getItem(7).setVisible(false);
        } else {
            navigationView.getMenu().getItem(7).setVisible(true);
        }

        adapter = new CalculationsAdapter(this);
        adapter.setListener(new CalculationsAdapter.CalcListener() {
            @Override
            public void onCalcSelected(Calculation calculation) {
                {
                    Intent intent = new Intent(ProjectEditActivity.this, CalcAddActivity.class);
                    intent.putExtra("isEdit", true);
                    intent.putExtra("calc", calculation);
                    intent.putExtra(IContract.ID, project.getId());
                    startActivityForResult(intent, 1);
                }
            }
        } );
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);
        project = (Project) getIntent().getSerializableExtra(IContract.PROJECT);
        id = getIntent().getIntExtra(IContract.ID, 0);
        if (project != null) {
            setCustomTitle(project.getmName());
            adapter.setItems(project.getCalculations());
        } else if (id != 0) {
            project = dbHelper.getSingleProject(id);
            setCustomTitle(project.getmName());
            adapter.setItems(project.getCalculations());
//            newCalculation = (Calculation) getIntent().getSerializableExtra(IContract.CALCULATION);
//            if (newCalculation != null) {
//                if (project.getCalculations() == null) {
//                    project.setCalculations(new ArrayList<>());
//                }
//                ArrayList<Calculation> temp = new ArrayList<>(project.getCalculations());
//                for (Calculation calculation : project.getCalculations()) {
//                    if (calculation.getMaterial().getType() == newCalculation.getMaterial().getType() &&
//                            calculation.getMaterial().getArticle().equals(newCalculation.getMaterial().getArticle())) {
//                        temp.remove(calculation);
//                        newCalculation.setAmount(newCalculation.getAmount() + calculation.getAmount());
//                        newCalculation.setFullPrice(Math.round(((newCalculation.getMaterial().getPrice()*newCalculation.getAmount())*
//                                (1+(newCalculation.getIsNds()?(newCalculation.getNds()/100.0):0)+
//                                        (newCalculation.getIsPercent()?newCalculation.getPercent()/100:0)))*100)/100.0);
//                    }
//                }
//                temp.add(newCalculation);
//                project.setCalculations(temp);
////                dbHelper.saveProject(project);
//                adapter.setItems(temp);
//            }
        }
        addCalc.setOnClickListener(adapter.getCheckBoxEnabled() ? deleteListener : addListener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_main:
                Intent intent5 = new Intent(ProjectEditActivity.this, LaunchActivity.class);
                startActivity(intent5);
                finish();
                break;
            case R.id.nav_na:
                Intent intent6 = new Intent(ProjectEditActivity.this, NotAvailableActivity.class);
                startActivity(intent6);
                finish();
                break;
            case R.id.nav_projects:
                Intent intent0 = new Intent(ProjectEditActivity.this, ProjectsActivity.class);
                startActivity(intent0);
                finish();
                break;
            case R.id.nav_count:
                Intent intent1 = new Intent(ProjectEditActivity.this, ProjectAddActivity.class);
                startActivity(intent1);
                finish();
                break;
            case R.id.nav_settings:
                Intent intent = new Intent(ProjectEditActivity.this, SettingsActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.nav_about:
                Intent intent4 = new Intent(ProjectEditActivity.this, AboutActivity.class);
                startActivity(intent4);
                finish();
                break;
            case R.id.nav_base:
                Intent intent2 = new Intent(ProjectEditActivity.this, BaseSelectActivity.class);
                startActivity(intent2);
                finish();
                break;
            case R.id.nav_pay:
                Intent intent3 = new Intent(ProjectEditActivity.this, BillingActivity.class);
                startActivity(intent3);
                finish();
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            project = dbHelper.getSingleProject(project.getId());
            adapter.setItems(project.getCalculations());
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
//            dbHelper.saveProject(project);
            Intent intent = new Intent(ProjectEditActivity.this, ProjectAddActivity.class);
            intent.putExtra(IContract.IS_EDIT, true);
            intent.putExtra(IContract.PROJECT, project);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
//        dbHelper.saveProject(project);
    }

    @Override
    protected void onResume() {
        super.onResume();
        recalculatePrice();
    }

    private void recalculatePrice() {
        double price = 0.0;
        ArrayList<Calculation> temp = project.getCalculations();
        if (temp != null) {
            if (temp.size() != 0) {
                for (Calculation calculation : temp) {
                    price += calculation.getFullPrice();
                }
            }
        }
        tvSum.setText(getResources().getString(R.string.rub, String.format("%.2f", price)));
        project.setmCost(price);
    }
}
