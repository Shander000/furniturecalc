package shander.furniturecalc.ui;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import shander.furniturecalc.CalcApp;
import shander.furniturecalc.R;
import shander.furniturecalc.adapters.ProjectsAdapter;
import shander.furniturecalc.db.DBHelper;
import shander.furniturecalc.entities.Project;
import shander.furniturecalc.interfaces.IContract;
import shander.furniturecalc.utils.LocalAppCompatActivity;

public class ProjectsActivity extends LocalAppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private ArrayList<Project> projects;
    private ProjectsAdapter adapter;
    private ImageButton btnEdit;
    private DBHelper dbHelper;

    @BindView(R.id.projects_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.add_project)
    FloatingActionButton addCat;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.empty_view)
    TextView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects);
        ButterKnife.bind(this);

        configureToolbar();
        setCustomTitle(getResources().getString(R.string.projects));

        dbHelper = CalcApp.getApplication().getDbHelper();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.getMenu().getItem(2).setVisible(false);
        if (CalcApp.getApplication().getPrefs().getBoolean(IContract.PREMIUM_ZERO, false)) {
            navigationView.getMenu().getItem(7).setVisible(false);
        } else {
            navigationView.getMenu().getItem(7).setVisible(true);
        }

        navigationView.setNavigationItemSelectedListener(this);

        btnEdit = toolbar.findViewById(R.id.title_btn_edit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    if (adapter.getItemCount() != 0) {
                        adapter.setCheckBoxEnabled(!adapter.getCheckBoxEnabled());
                        addCat.setImageResource(adapter.getCheckBoxEnabled() ? android.support.design.R.drawable.abc_ic_clear_material : R.drawable.ic_add);
                        addCat.setOnClickListener(adapter.getCheckBoxEnabled() ? deleteListener : addListener);
                        addCat.setTranslationX(adapter.getCheckBoxEnabled() ? -20f : 0);
                        addCat.setTranslationY(adapter.getCheckBoxEnabled() ? -5f : 0);
                    }
                }
            }
        } );

        adapter = new ProjectsAdapter(this);
        adapter.setOnItemClickListener(new ProjectsAdapter.Listener() {
            @Override
            public void onItemClick(View view, Project project)  {
                Intent intent = new Intent(ProjectsActivity.this, ProjectAddActivity.class);
                intent.putExtra(IContract.IS_EDIT, true);
                intent.putExtra(IContract.PROJECT, project);
                startActivity(intent);
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);
        projects = null;
        addCat.setOnClickListener(adapter.getCheckBoxEnabled() ? deleteListener : addListener);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_main:
                Intent intent5 = new Intent(ProjectsActivity.this, LaunchActivity.class);
                startActivity(intent5);
                finish();
                break;
            case R.id.nav_na:
                Intent intent6 = new Intent(ProjectsActivity.this, NotAvailableActivity.class);
                startActivity(intent6);
                finish();
                break;
            case R.id.nav_count:
                Intent intent1 = new Intent(ProjectsActivity.this, ProjectAddActivity.class);
                startActivity(intent1);
                finish();
                break;
            case R.id.nav_base:
                Intent intent2 = new Intent(ProjectsActivity.this, BaseSelectActivity.class);
                startActivity(intent2);
                finish();
                break;
            case R.id.nav_settings:
                Intent intent = new Intent(ProjectsActivity.this, SettingsActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.nav_about:
                Intent intent4 = new Intent(ProjectsActivity.this, AboutActivity.class);
                startActivity(intent4);
                finish();
                break;
            case R.id.nav_pay:
                Intent intent3 = new Intent(ProjectsActivity.this, BillingActivity.class);
                startActivity(intent3);
                finish();
                break;
        }
        return false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        projects = dbHelper.getAllProjects();
        Log.wtf("PROJECTS START", (projects != null ? projects.size() : 0) + "|");
        checkIsEmpty();
    }

    private void restoreView() {
        addCat.setImageResource(R.drawable.ic_add);
        addCat.setOnClickListener(addListener);
        addCat.setTranslationX(0);
        addCat.setTranslationY(0);
    }

    private View.OnClickListener addListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            {
                Intent intent = new Intent(ProjectsActivity.this, ProjectAddActivity.class);
                startActivity(intent);
            }
        }
    } ;

    private View.OnClickListener deleteListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            boolean[] toDelete = adapter.getChecked();
            for (int i = toDelete.length-1; i >= 0; i--) {
                if (toDelete[i]) {
                    dbHelper.deleteProject(projects.get(i).getId());
                    projects.remove(i);
                }
            }
            if (projects.size() != 0) {
                adapter.setItems(projects);
                btnEdit.performClick();
            } else {
                checkIsEmpty();
            }
        }
    };

    private void checkIsEmpty() {
        if (projects != null && projects.size() != 0) {
            adapter.setItems(projects);
            recyclerView.setVisibility(View.VISIBLE);
            view.setVisibility(View.GONE);
            adapter.setCheckBoxEnabled(false);
        } else {
            recyclerView.setVisibility(View.GONE);
            view.setVisibility(View.VISIBLE);
        }
        restoreView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
