package shander.furniturecalc.ui;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import shander.furniturecalc.CalcApp;
import shander.furniturecalc.R;
import shander.furniturecalc.db.DBHelper;
import shander.furniturecalc.dialogs.ConfirmationDialog;
import shander.furniturecalc.entities.BaseCatalogue;
import shander.furniturecalc.entities.Material;
import shander.furniturecalc.interfaces.IContract;
import shander.furniturecalc.utils.JSONHelper;
import shander.furniturecalc.utils.LocalAppCompatActivity;

public class FurnitureAddActivity extends LocalAppCompatActivity implements ConfirmationDialog.Listener{

    @BindView(R.id.title)
    TextInputEditText titleEdit;
    @BindView(R.id.price)
    TextInputEditText priceEdit;
    @BindView(R.id.article)
    TextInputEditText articleEdit;
    @BindView(R.id.units)
    Spinner unitsSpinner;
    @BindView(R.id.notes)
    TextInputEditText notesEdit;
    @BindView(R.id.accept)
    FloatingActionButton acceptButton;

    @OnClick(R.id.accept)
    void onClick(){
        if (checkEntred()) {
            if (!CalcApp.getApplication().getPrefs().getBoolean(IContract.PREMIUM_ZERO, false) &&
                    (!material.getTitle().equals(titleEdit.getText().toString()) ||
                            !material.getArticle().equals(articleEdit.getText().toString()) ||
                            !material.getComment().equals(notesEdit.getText().toString()) ||
                            !material.getUnit().equals(unitsSpinner.getSelectedItem()) ||
                            material.getPrice() != (Double.parseDouble(priceEdit.getText().toString())))) {
                ConfirmationDialog.show(FurnitureAddActivity.this, 11,
                        getResources().getString(R.string.warning), getResources().getString(R.string.paidMessage));
            } else {
                material.setTitle(titleEdit.getText().toString());
                material.setArticle(articleEdit.getText().toString());
                material.setComment(notesEdit.getText().toString());
                material.setUnit((String) unitsSpinner.getSelectedItem());
                material.setPrice(Double.parseDouble(priceEdit.getText().toString()));
                dbHelper.saveMaterial(material);
                Intent intent = new Intent(FurnitureAddActivity.this, BaseSelectActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    private Material material;
    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_base_add);

        ButterKnife.bind(this);

        configureToolbar();
        setCustomTitle(getResources().getString(R.string.edit_material));

        ImageButton btnEdit = toolbar.findViewById(R.id.title_btn_edit);
        btnEdit.setVisibility(View.GONE);

        dbHelper = CalcApp.getApplication().getDbHelper();

        material = (Material) getIntent().getSerializableExtra(IContract.MATERIAL);
        ArrayAdapter<?> adapter =
                ArrayAdapter.createFromResource(this, R.array.units, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        unitsSpinner.setAdapter(adapter);
        if (material != null) {
            titleEdit.setText(material.getTitle());
            priceEdit.setText(String.valueOf(material.getPrice()));
            articleEdit.setText(material.getArticle());
            switch (material.getUnit()){
                case "шт.":
                    unitsSpinner.setSelection(0);
                    break;
                case "компл.":
                    unitsSpinner.setSelection(1);
                    break;
                case "кв.м.":
                    unitsSpinner.setSelection(2);
                    break;
                case "п.м.":
                    unitsSpinner.setSelection(3);
                    break;
                case "лист":
                    unitsSpinner.setSelection(4);
                    break;
                case "упак":
                    unitsSpinner.setSelection(5);
                    break;
                case "литр":
                    unitsSpinner.setSelection(6);
                    break;
                case "кг":
                    unitsSpinner.setSelection(7);
                    break;
            }
            notesEdit.setText(material.getComment());
        } else {
            material = new Material();
            material.setType(getIntent().getIntExtra(IContract.CATALOGUE, 0));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private boolean checkEntred(){
        if (TextUtils.isEmpty(titleEdit.getText())){
            titleEdit.setError("Введите значение");
            return false;
        } else if (TextUtils.isEmpty(priceEdit.getText())){
            priceEdit.setError("Введите значение");
            return false;
        } else if (TextUtils.isEmpty(articleEdit.getText())){
            articleEdit.setError("Введите значение");
            return false;
        } else return true;
    }

    @Override
    public void onConfirmed(int messageId) {
        Intent intent2 = new Intent(FurnitureAddActivity.this, BillingActivity.class);
        intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent2);
        finish();
    }
}
