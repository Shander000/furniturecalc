package shander.furniturecalc.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import shander.furniturecalc.CalcApp;
import shander.furniturecalc.R;
import shander.furniturecalc.interfaces.IContract;
import shander.furniturecalc.utils.LocalAppCompatActivity;

public class SettingsActivity extends LocalAppCompatActivity {

    @BindView(R.id.et_nds)
    EditText etNds;

    @BindView(R.id.et_coeff)
    EditText etCoeff;

    @BindView(R.id.tv_change_font)
    Button tvFont;

//    @BindView(R.id.tv_change_font_size)
//    Button tvSize;

    @BindView(R.id.btn_save)
    Button btnSave;

    @BindView(R.id.screen_switch)
    Switch keepScreenSwitch;

    private int nds;
    private Float coeff;

    @OnCheckedChanged(R.id.screen_switch)
    void check(boolean checked) {
        CalcApp.getApplication().getEditor().putBoolean(IContract.KEEP_SCREEN, checked).apply();
    }

    @OnClick({R.id.tv_change_font, R.id.btn_save})
    void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_change_font:
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(SettingsActivity.this);
                builderSingle.setIcon(R.drawable.ic_info_black_24dp);
                builderSingle.setTitle(getResources().getString(R.string.choose_font));

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                        SettingsActivity.this,
                        android.R.layout.select_dialog_singlechoice);
                arrayAdapter.addAll(getResources().getStringArray(R.array.text_font));

                builderSingle.setNegativeButton(getResources().getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builderSingle.setAdapter(arrayAdapter,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                CalcApp.getApplication().overrideFont(SettingsActivity.this, arrayAdapter.getItem(which));
                                dialog.dismiss();
                            }
                        });
                builderSingle.show();
                break;
//            case R.id.tv_change_font_size:
//                AlertDialog.Builder builderSingleSize = new AlertDialog.Builder(SettingsActivity.this);
//                builderSingleSize.setIcon(R.drawable.ic_info_black_24dp);
//                builderSingleSize.setTitle(getResources().getString(R.string.choose_font));
//
//                final ArrayAdapter<String> arrayAdapterSizes = new ArrayAdapter<>(
//                        SettingsActivity.this,
//                        android.R.layout.select_dialog_singlechoice);
//                arrayAdapterSizes.addAll(getResources().getStringArray(R.array.font_size));
//
//                builderSingleSize.setNegativeButton(getResources().getString(R.string.cancel),
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                            }
//                        });
//                builderSingleSize.setAdapter(arrayAdapterSizes,
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                                switch (arrayAdapterSizes.getItem(which)) {
//                                    case "Нормальный":
//                                        CalcApp.getApplication().overrideFontSize("normal");
//                                        break;
//                                    case "Увеличенный":
//                                        CalcApp.getApplication().overrideFontSize("large");
//                                        break;
//                                    case "Уменьшенный":
//                                        CalcApp.getApplication().overrideFontSize("small");
//                                        break;
//                                }
//                                dialog.dismiss();
//                            }
//                        });
//                builderSingleSize.show();
//                break;
            case R.id.btn_save:
                if (!TextUtils.isEmpty(etCoeff.getText().toString())) {
                    try {
                        coeff = Float.parseFloat(etCoeff.getText().toString());
                    } catch (final NumberFormatException e) {
                        coeff = 0.0f;
                    }
                    CalcApp.getApplication().getEditor().putFloat(IContract.COEFF, coeff).apply();
                }
                if (!TextUtils.isEmpty(etNds.getText().toString())) {
                    nds = Integer.parseInt(etNds.getText().toString());
                    CalcApp.getApplication().getEditor().putInt(IContract.NDS, nds).apply();
                }
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        configureToolbar();

        setCustomTitle(R.string.settings);

        etNds.setText(String.valueOf(CalcApp.getApplication().getPrefs().getInt(IContract.NDS, 0)));
        etCoeff.setText(String.valueOf(CalcApp.getApplication().getPrefs().getFloat(IContract.COEFF, 0)));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SettingsActivity.this, LaunchActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }
}
