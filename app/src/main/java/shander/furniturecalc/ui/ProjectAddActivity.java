package shander.furniturecalc.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import shander.furniturecalc.CalcApp;
import shander.furniturecalc.R;
import shander.furniturecalc.db.DBHelper;
import shander.furniturecalc.entities.Project;
import shander.furniturecalc.interfaces.IContract;
import shander.furniturecalc.utils.JSONHelper;
import shander.furniturecalc.utils.LocalAppCompatActivity;

public class ProjectAddActivity extends LocalAppCompatActivity {

    private Boolean isEdit;
    private String mName = "";
    private String mFIO = "";
    private Double mCost = 0.0;
    private String mAddress = "";
    private String mPhone = "";
    private String mNote = "";
    private Project project;
    private Boolean isViewOnly;
    private DBHelper dbHelper;

    @BindView(R.id.et_name)
    EditText mNameEdit;
    @BindView(R.id.et_fio)
    EditText mFIOEdit;
    @BindView(R.id.et_cost)
    EditText mCostEdit;
    @BindView(R.id.et_address)
    EditText mAddressEdit;
    @BindView(R.id.et_phone)
    EditText mPhoneEdit;
    @BindView(R.id.et_note)
    EditText mNoteEdit;
    @BindView(R.id.accept)
    FloatingActionButton mAcceptButton;

    @OnClick(R.id.accept)
    void save() {
        mName = mNameEdit.getText().toString();
        mFIO = mFIOEdit.getText().toString();
        try {
            mCost = Double.parseDouble(mCostEdit.getText().toString().replace(",", "."));
        } catch (NumberFormatException e) {
            mCost = 0.0;
        }
        mAddress = mAddressEdit.getText().toString();
        mPhone = mPhoneEdit.getText().toString();
        mNote = mNoteEdit.getText().toString();
        if (mName.equals("")) {
            Toast.makeText(ProjectAddActivity.this, R.string.enter_project_name, Toast.LENGTH_LONG).show();
            mNameEdit.setError(getString(R.string.required_field));
        } else {

            if (!isEdit) {
                project = new Project();
                Random r = new Random();
                project.setId(r.nextInt(100000) + r.nextInt(100000) + r.nextInt(256000));
            }
            project.setmName(mName);
            project.setmAddress(mAddress);
            project.setmCost(mCost);
            project.setmFIO(mFIO);
            project.setmNote(mNote);
            project.setmPhone(mPhone);
            dbHelper.saveProject(project);
            Intent intent = new Intent(ProjectAddActivity.this, ProjectEditActivity.class);
            intent.putExtra(IContract.PROJECT, project);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_project);
        ButterKnife.bind(this);
        configureToolbar();

        dbHelper = CalcApp.getApplication().getDbHelper();

        isEdit = getIntent().getBooleanExtra(IContract.IS_EDIT, false);
        setCustomTitle(isEdit ? R.string.edit_project : R.string.add_project);
        if (isEdit) {
            project = (Project) getIntent().getSerializableExtra(IContract.PROJECT);
            mNameEdit.setText(project.getmName());
            mCostEdit.setText(String.format("%.2f", project.getmCost()));
            mAddressEdit.setText(project.getmAddress());
            mFIOEdit.setText(project.getmFIO());
            mNoteEdit.setText(project.getmNote());
            mPhoneEdit.setText(project.getmPhone());
        }

        mCostEdit.setEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }
}
