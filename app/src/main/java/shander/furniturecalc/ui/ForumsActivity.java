package shander.furniturecalc.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import shander.furniturecalc.R;
import shander.furniturecalc.utils.LocalAppCompatActivity;

public class ForumsActivity extends LocalAppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forums);
        configureToolbar();
        setCustomTitle(getString(R.string.forums));
        toolbar.findViewById(R.id.title_btn_edit).setVisibility(View.GONE);
        ImageButton mebf = findViewById(R.id.btn_mebf);
        mebf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://mebforum.ru/topic/6606-mebelnyi-kalkuliator-dlia-android/#entry34333"));
                startActivity(browserIntent);
            }
        } );
        ImageButton prom = findViewById(R.id.btn_prom);
        prom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://promebelclub.ru/forum/showthread.php?t=5501&page=12"));
                startActivity(browserIntent);
            }
        } );
        ImageButton sdel = findViewById(R.id.btn_sdel);
        sdel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://forum.sdelaimebel.ru/topic/31772-mebelnyiy-kalkulyator-dlya-android/#comment-379005"));
                    startActivity(browserIntent);
                }
            }
        } );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ForumsActivity.this, LaunchActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
