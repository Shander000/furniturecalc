package shander.furniturecalc.ui;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import shander.furniturecalc.CalcApp;
import shander.furniturecalc.R;
import shander.furniturecalc.adapters.DatabaseAdapter;
import shander.furniturecalc.adapters.FurnitureAdapter;
import shander.furniturecalc.db.DBHelper;
import shander.furniturecalc.dialogs.ConfirmationDialog;
import shander.furniturecalc.dialogs.InputDialog;
import shander.furniturecalc.entities.BaseCatalogue;
import shander.furniturecalc.entities.Material;
import shander.furniturecalc.interfaces.IContract;
import shander.furniturecalc.utils.LocalAppCompatActivity;

public class BaseSelectActivity extends LocalAppCompatActivity implements InputDialog.Listener, NavigationView.OnNavigationItemSelectedListener,
        ConfirmationDialog.Listener {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.empty_view)
    TextView view;
    @BindView(R.id.add_catalogue)
    FloatingActionButton addCat;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    private DatabaseAdapter adapter;
    private ArrayList<BaseCatalogue> catalogues;
    private boolean isFromProject = false;
    private int id;
    private BaseCatalogue temp;
    private FurnitureAdapter innerAdapter;
    private ImageButton btnEdit;
    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_base);
        ButterKnife.bind(this);

        configureToolbar();
        setCustomTitle(getResources().getString(R.string.database_title));

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        dbHelper = CalcApp.getApplication().getDbHelper();

        final View.OnClickListener addListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    if (CalcApp.getApplication().getPrefs().getBoolean(IContract.PREMIUM_ZERO, false)) {
                        if (recyclerView.getAdapter() instanceof DatabaseAdapter) {
                            InputDialog.show(BaseSelectActivity.this, 0, "");
                        } else if (recyclerView.getAdapter() instanceof FurnitureAdapter) {
                            Intent intent = new Intent(BaseSelectActivity.this, FurnitureAddActivity.class);
                            intent.putExtra(IContract.CATALOGUE, temp.getCatId());
                            startActivity(intent);
                        }
                    } else {
                        ConfirmationDialog.show(BaseSelectActivity.this, 11,
                                getResources().getString(R.string.warning), getResources().getString(R.string.paidMessage));
                    }
                }
            }
        } ;

        final View.OnClickListener deleteListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    if (CalcApp.getApplication().getPrefs().getBoolean(IContract.PREMIUM_ZERO, false)) {
                        if (recyclerView.getAdapter() instanceof DatabaseAdapter) {
                            boolean[] toDelete = adapter.getChecked();
                            for (int i = toDelete.length - 1; i >= 0; i--) {
                                if (toDelete[i]) {
                                    dbHelper.deleteCategory(catalogues.get(i).getCatId());
                                    catalogues = dbHelper.getAllCategories();
                                }
                            }
                            adapter.setItems(catalogues);
                        } else if (recyclerView.getAdapter() instanceof FurnitureAdapter) {
                            boolean[] toDelete = innerAdapter.getChecked();

                            ArrayList<Material> materials = temp.getMaterials();
//                        materials = new ArrayList<>(Arrays.asList(temp.getMaterials()));
                            for (int i = toDelete.length - 1; i >= 0; i--) {
                                if (toDelete[i]) {
                                    dbHelper.deleteMaterial(materials.get(i).getId());
                                    materials.remove(i);
                                }
                            }
                            temp.setMaterials(materials);
                            innerAdapter.setItems(temp);
                            dbHelper.saveCategory(temp);
                        }
                        btnEdit.performClick();
                    } else {
                        ConfirmationDialog.show(BaseSelectActivity.this, 11,
                                getResources().getString(R.string.warning), getResources().getString(R.string.paidMessage));
                    }
                }
            }
        } ;

        btnEdit = toolbar.findViewById(R.id.title_btn_edit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    if (CalcApp.getApplication().getPrefs().getBoolean(IContract.PREMIUM_ZERO, false)) {
                        if (recyclerView.getAdapter() instanceof DatabaseAdapter) {
                            adapter.setCheckBoxEnabled(!adapter.getCheckBoxEnabled());
                            addCat.setOnClickListener(adapter.getCheckBoxEnabled() ? deleteListener : addListener);
                            addCat.setImageResource(adapter.getCheckBoxEnabled() ? android.support.design.R.drawable.abc_ic_clear_material : R.drawable.ic_add);
                            addCat.setTranslationX(adapter.getCheckBoxEnabled() ? -20f : 0);
                            addCat.setTranslationY(adapter.getCheckBoxEnabled() ? -5f : 0);
                        } else {
                            innerAdapter.setCheckBoxEnabled(!innerAdapter.getCheckBoxEnabled());
                            addCat.setOnClickListener(innerAdapter.getCheckBoxEnabled() ? deleteListener : addListener);
                            addCat.setImageResource(innerAdapter.getCheckBoxEnabled() ? android.support.design.R.drawable.abc_ic_clear_material : R.drawable.ic_add);
                            addCat.setTranslationX(innerAdapter.getCheckBoxEnabled() ? -20f : 0);
                            addCat.setTranslationY(innerAdapter.getCheckBoxEnabled() ? -5f : 0);
                        }
                    } else {
                        ConfirmationDialog.show(BaseSelectActivity.this, 11,
                                getResources().getString(R.string.warning), getResources().getString(R.string.paidMessage));
                    }
                }
            }
        } );

        navigationView.setNavigationItemSelectedListener(this);
        if (CalcApp.getApplication().getPrefs().getBoolean(IContract.PREMIUM_ZERO, false)) {
            navigationView.getMenu().getItem(7).setVisible(false);
        } else {
            navigationView.getMenu().getItem(7).setVisible(true);
        }

        isFromProject = getIntent().getBooleanExtra(IContract.FROM_PROJECT, false);
        btnEdit.setVisibility(isFromProject ? View.GONE : View.VISIBLE);
        addCat.setVisibility(isFromProject ? View.GONE : View.VISIBLE);
        id = getIntent().getIntExtra(IContract.ID, 0);
        navigationView.getMenu().getItem(3).setVisible(false);
        adapter = new DatabaseAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new DatabaseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, BaseCatalogue catalogue) {
                {
                    innerAdapter = new FurnitureAdapter(BaseSelectActivity.this);
                    temp = catalogue;
                    innerAdapter.setItems(catalogue);
                    recyclerView.removeAllViews();
                    recyclerView.setAdapter(innerAdapter);
                    BaseSelectActivity.this.setCustomTitle(temp.getName());
                    innerAdapter.setOnItemClickListener(new FurnitureAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, Material material) {
                            {
                                if (isFromProject) {
                                    Intent intent = new Intent(BaseSelectActivity.this, CalcAddActivity.class);
                                    intent.putExtra(IContract.MATERIAL, material);
                                    intent.putExtra(IContract.ID, id);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Intent intent = new Intent(BaseSelectActivity.this, FurnitureAddActivity.class);
                                    intent.putExtra(IContract.MATERIAL, material);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        }
                    } );
                }
            }
        } );

        addCat.setOnClickListener(adapter.getCheckBoxEnabled() ? deleteListener : addListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        catalogues = dbHelper.getAllCategories();
        if (catalogues == null) {
            view.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            adapter.setItems(catalogues);
        }
        if (innerAdapter != null) {
            for (BaseCatalogue catalogue : catalogues) {
                if (catalogue.getCatId() == temp.getCatId()) {
                    innerAdapter.setItems(catalogue);
                    temp = catalogue;
                }
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_main:
                Intent intent5 = new Intent(BaseSelectActivity.this, LaunchActivity.class);
                startActivity(intent5);
                finish();
                break;
            case R.id.nav_na:
                Intent intent6 = new Intent(BaseSelectActivity.this, NotAvailableActivity.class);
                startActivity(intent6);
                finish();
                break;
            case R.id.nav_projects:
                Intent intent0 = new Intent(BaseSelectActivity.this, ProjectsActivity.class);
                startActivity(intent0);
                finish();
                break;
            case R.id.nav_count:
                Intent intent1 = new Intent(BaseSelectActivity.this, ProjectAddActivity.class);
                startActivity(intent1);
                finish();
                break;
            case R.id.nav_settings:
                Intent intent = new Intent(BaseSelectActivity.this, SettingsActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.nav_about:
                Intent intent4 = new Intent(BaseSelectActivity.this, AboutActivity.class);
                startActivity(intent4);
                finish();
                break;
            case R.id.nav_pay:
                Intent intent2 = new Intent(BaseSelectActivity.this, BillingActivity.class);
                startActivity(intent2);
                finish();
                break;
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (recyclerView.getAdapter() instanceof FurnitureAdapter) {
            recreate();
        } else
            super.onBackPressed();
    }

    @Override
    public void onInputEnter(String input) {
        BaseCatalogue catalogue = new BaseCatalogue();
        catalogue.setName(input);
        catalogue.setCatId(new Random().nextInt(100000) + 10000 + new Random().nextInt(10000) - new Random().nextInt(10000));
//        catalogue.setMaterials(new Material[0]);
        catalogues.add(catalogue);
        adapter.setItems(catalogues);
        dbHelper.saveCategory(catalogue);
    }

    @Override
    public void onConfirmed(int messageId) {
        Intent intent2 = new Intent(BaseSelectActivity.this, BillingActivity.class);
        intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent2);
        finish();
    }
}
