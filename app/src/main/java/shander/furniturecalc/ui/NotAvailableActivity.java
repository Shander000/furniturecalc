package shander.furniturecalc.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import shander.furniturecalc.CalcApp;
import shander.furniturecalc.R;
import shander.furniturecalc.adapters.NotAvailAdapter;
import shander.furniturecalc.db.DBHelper;
import shander.furniturecalc.dialogs.MessageDialog;
import shander.furniturecalc.entities.Project;
import shander.furniturecalc.utils.LocalAppCompatActivity;

public class NotAvailableActivity extends LocalAppCompatActivity implements NotAvailAdapter.NAListener, MessageDialog.Listener {

    private NotAvailAdapter adapter;
    private RecyclerView listView;
    private FloatingActionButton deleteBtn;
    private ArrayList<Integer> idsToDelete = new ArrayList<>();
    private DBHelper dbHelper;
    private TextView summary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_not_available);configureToolbar();
        setCustomTitle(getString(R.string.not_available));
        summary = findViewById(R.id.sum);
        dbHelper = CalcApp.getApplication().getDbHelper();
        deleteBtn = findViewById(R.id.na_fab);
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (Integer id : adapter.getChecked()) {
                    if (id != null) {
                        if (id != 0) {
                            idsToDelete.add(id);
                        }
                    }
                }
                if (idsToDelete.size() != 0) {
                    for (int i : idsToDelete) {
                        dbHelper.deleteNAFromMaterialId(i);
                    }
                }
                adapter.updateData();
                toolbar.findViewById(R.id.title_btn_edit).performClick();
            }
        });
        toolbar.findViewById(R.id.title_btn_edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (adapter.getItemCount() != 0) {
                    adapter.setmIsDeleteEnabled(!adapter.ismIsDeleteEnabled());
                    deleteBtn.setVisibility(adapter.ismIsDeleteEnabled() ? View.VISIBLE : View.GONE);
                }
            }
        });
        listView = findViewById(R.id.na_list);
        adapter = new NotAvailAdapter(getLayoutInflater(),this, this);
        listView.setAdapter(adapter);
        listView.setLayoutManager(new LinearLayoutManager(this));
        listView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
            Intent intent = new Intent(NotAvailableActivity.this, LaunchActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
    }

    @Override
    public void onItemsLoaded(double sum) {
        summary.setText(String.format("%.2f", sum));
    }

    @Override
    public void onNAClick(LinkedHashMap<Project, Double> projects, String title, String unit) {
        StringBuilder b = new StringBuilder();
        for (Project p : new ArrayList<>(projects.keySet())) {
            b.append(p.getmName());
            b.append(": ");
            b.append(projects.get(p));
            b.append(" ");
            b.append(unit);
            b.append("\n");
        }
        MessageDialog.show(this, 12, title, b.toString());
    }

    @Override
    public void onMessageDialogDismissed(int messageId) {

    }
}
