package shander.furniturecalc.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import shander.furniturecalc.CalcApp;
import shander.furniturecalc.R;
import shander.furniturecalc.interfaces.IContract;
import shander.furniturecalc.utils.LocalAppCompatActivity;

public class LaunchActivity extends LocalAppCompatActivity {

    @BindView(R.id.btn_settings)
    Button mSettings;

    @BindView(R.id.btn_base)
    Button mBase;

    @BindView(R.id.btn_count)
    Button mCount;

    @BindView(R.id.btn_projects)
    Button mProjects;

    @BindView(R.id.btn_about)
    Button mAbout;

    @BindView(R.id.btn_na)
    Button mNA;

    @BindView(R.id.btn_forums)
    Button mForums;

    @OnClick({R.id.btn_settings, R.id.btn_base, R.id.btn_count, R.id.btn_projects, R.id.btn_about, R.id.btn_na, R.id.btn_forums})
    void onClick(Button view) {
        switch (view.getId()){
            case R.id.btn_settings:
                Intent intent = new Intent(LaunchActivity.this, SettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_count:
                Intent intent1 = new Intent(LaunchActivity.this, ProjectAddActivity.class);
                startActivity(intent1);
                break;
            case R.id.btn_base:
                Intent intent2 = new Intent(LaunchActivity.this, BaseSelectActivity.class);
                startActivity(intent2);
                break;
            case R.id.btn_projects:
                Intent intent3 = new Intent(LaunchActivity.this, ProjectsActivity.class);
                startActivity(intent3);
                break;
            case R.id.btn_about:
                Intent intent4 = new Intent(LaunchActivity.this, AboutActivity.class);
                startActivity(intent4);
                break;
            case R.id.btn_na:
                Intent intent5 = new Intent(LaunchActivity.this, NotAvailableActivity.class);
                startActivity(intent5);
                break;
            case R.id.btn_forums:
                Intent intent6 = new Intent(LaunchActivity.this, ForumsActivity.class);
                startActivity(intent6);
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        ButterKnife.bind(this);

        Log.wtf("SETTINGS", CalcApp.getApplication().getPrefs().getBoolean(IContract.PREMIUM_ZERO, false) + "|");

        configureToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        setCustomTitle(R.string.app_name);

    }

}
