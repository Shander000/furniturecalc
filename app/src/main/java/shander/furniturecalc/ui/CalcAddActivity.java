package shander.furniturecalc.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import shander.furniturecalc.CalcApp;
import shander.furniturecalc.R;
import shander.furniturecalc.db.DBHelper;
import shander.furniturecalc.dialogs.ConfirmationDialog;
import shander.furniturecalc.dialogs.MessageDialog;
import shander.furniturecalc.entities.Calculation;
import shander.furniturecalc.entities.Material;
import shander.furniturecalc.interfaces.IContract;
import shander.furniturecalc.utils.JSONHelper;
import shander.furniturecalc.utils.LocalAppCompatActivity;

public class CalcAddActivity extends LocalAppCompatActivity implements ConfirmationDialog.Listener{

    @BindView(R.id.title)
    TextView calcTitle;
    @BindView(R.id.amount)
    TextInputEditText amountEdit;
    @BindView(R.id.price_edit)
    TextInputEditText priceEdit;
    @BindView(R.id.price)
    TextView priceValue;
    @BindView(R.id.units)
    TextView unitsValue;
    @BindView(R.id.percentValue)
    EditText percentEdit;
    @BindView(R.id.percent)
    CheckBox percentSelection;
    @BindView(R.id.ndsValue)
    EditText ndsValue;
    @BindView(R.id.nds)
    CheckBox ndsSelection;
    @BindView(R.id.notes)
    TextView notesEdit;
    @BindView(R.id.accept)
    FloatingActionButton acceptFab;
    @BindView(R.id.calc_add_progress)
    ProgressBar progressBar;
    @BindView(R.id.calc_add_main)
    View mainView;
    @BindView(R.id.na_check)
    CheckBox naCheck;

    private Material material;
    private Double count = 0.0;
    private int nds = 0;
    private double percent = 0.0;
    private boolean isPercent;
    private boolean isNds;
    private String comment;
    private int projectId;
    private boolean isEdit;
    private int uId = 0;
    private boolean isNA = true;
    private boolean isNAChanged = false;

    private DBHelper dbHelper;

    @OnClick(R.id.accept)
    void accept() {
        if (count == 0) {
            MessageDialog.show(this, 1, getResources().getString(R.string.warning), getResources().getString(R.string.count_null));
        } else {
            progressBar.setVisibility(View.VISIBLE);
            mainView.setVisibility(View.INVISIBLE);
            Calculation calculation = new Calculation();
            calculation.setMaterial(material);
            if (uId != 0) {
                calculation.setuId(uId);
            }
            calculation.setAmount(count);
            calculation.setComment(comment);
            calculation.setFullPrice(Math.round(((material.getPrice()*count)*(1+(isNds?(nds/100.0):0)+(isPercent?percent/100:0)))*100)/100.0);
            calculation.setIsNds(isNds);
            calculation.setIsPercent(isPercent);
            if (isNds) {
                calculation.setNds(nds);
            }
            if (isPercent) {
                calculation.setPercent(percent);
            }
            calculation.setProjectId(projectId);
            calculation.setTitle(material.getTitle());
            calculation.setMaterialAvailable(!isNA);
            if (!isNA && !isNAChanged) {
                dbHelper.deleteNAFromCalc(calculation.getProjectId(), material.getId());
            }
            dbHelper.saveCalculation(calculation);
            if (isEdit) {
                Intent intent = new Intent();
                intent.putExtra(IContract.CALCULATION, calculation);
                intent.putExtra(IContract.ID, projectId);
                setResult(RESULT_OK, intent);
                finish();
            } else {
                Intent intent = new Intent(CalcAddActivity.this, ProjectEditActivity.class);
                intent.putExtra(IContract.CALCULATION, calculation);
                intent.putExtra(IContract.ID, projectId);
                startActivity(intent);
                finish();
            }
        }
    }

    @OnCheckedChanged({R.id.percent, R.id.nds, R.id.na_check})
    void onSelect(CompoundButton button, boolean isChecked) {
        switch (button.getId()) {
            case R.id.percent:
                isPercent = isChecked;
                percentEdit.setEnabled(isChecked);
                if (isPercent) {
                    percentEdit.setText(String.valueOf(percent));
                }
                break;
            case R.id.nds:
                isNds = isChecked;
                ndsValue.setEnabled(isChecked);
                if (isNds) {
                    ndsValue.setText(String.valueOf(nds));
                }
                break;
            case R.id.na_check:
                isNA = isChecked;
                isNAChanged = !isNAChanged;
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calc_add);
        ButterKnife.bind(this);

        configureToolbar();

        dbHelper = CalcApp.getApplication().getDbHelper();

        isEdit = getIntent().getBooleanExtra("isEdit", false);
        if (isEdit) {
            Calculation calculation = (Calculation)getIntent().getSerializableExtra("calc");
            if (calculation != null) {
                uId = calculation.getuId();
                nds = calculation.getNds();
                percent = calculation.getPercent();
                material = calculation.getMaterial();
                isNds = calculation.getIsNds();
                isPercent = calculation.getIsPercent();
                comment = calculation.getComment();
                count = calculation.getAmount();
                isNA = !calculation.isMaterialAvailable();
                priceValue.setText(getResources().getString(R.string.price_is, String.valueOf((double)Math.round(((material.getPrice()*count)*(1+(isNds?(nds/100.0):0)+(isPercent?percent/100:0)))*100)/100.0)));
                if (isNds) {
                    ndsValue.setText(String.format(Locale.getDefault(), "%d%%", nds));
                    ndsSelection.setChecked(true);
                }
                if (isPercent) {
                    percentEdit.setText(String.valueOf(percent));
                    percentSelection.setChecked(true);
                }
                amountEdit.setText(String.valueOf(count));
                unitsValue.setText(material.getUnit());
                calcTitle.setText(String.format("%s\n(%s)", material.getTitle(), material.getArticle()));
            } else {
                finish();
            }
        } else {
            uId = dbHelper.getAllCalculations().size() + 1;
            nds = CalcApp.getApplication().getPrefs().getInt(IContract.NDS, 0);
            percent = (double) CalcApp.getApplication().getPrefs().getFloat(IContract.COEFF, 0.0f);
            material = (Material) getIntent().getSerializableExtra(IContract.MATERIAL);
            priceValue.setText(getResources().getString(R.string.price_is, String.valueOf(0)));
            unitsValue.setText(material.getUnit());
            calcTitle.setText(String.format("%s\n(%s)", material.getTitle(), material.getArticle()));

            if (CalcApp.getApplication().getPrefs().getInt(IContract.NDS, 0) != 0) {
                ndsValue.setText(String.format(Locale.getDefault(), "%d%%", nds));
                ndsSelection.setChecked(true);
            } else {
                ndsSelection.setChecked(false);
            }
            if (CalcApp.getApplication().getPrefs().getFloat(IContract.COEFF, 0) != 0.0) {
                percentEdit.setText(String.valueOf(percent));
                percentSelection.setChecked(true);
            } else
            {
                percentSelection.setChecked(false);
            }
        }

        notesEdit.setText(material.getComment());

        naCheck.setChecked(isNA);

        amountEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try{
                    count = Double.parseDouble(amountEdit.getText().toString());
                } catch (final NumberFormatException e) {
                    count = 0.0;
                }
                priceValue.setText(getResources().getString(R.string.price_is, String.valueOf(Math.round(((material.getPrice()*count)*(1+(isNds?(nds/100.0):0)+(isPercent?percent/100:0)))*100)/100.0)));
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });
        percentEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try{
                    percent = Double.parseDouble(percentEdit.getText().toString());
                } catch (final NumberFormatException e) {
                    percent = 0.0;
                }
                priceValue.setText(getResources().getString(R.string.price_is, String.valueOf((double)Math.round(((material.getPrice()*count)*(1+(isNds?(nds/100.0):0)+(isPercent?percent/100:0)))*100)/100.0)));
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });
        ndsValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!TextUtils.isEmpty(ndsValue.getText())) {
                    nds = Integer.parseInt(ndsValue.getText().toString().replace("%", ""));
                }
                priceValue.setText(getResources().getString(R.string.price_is, String.valueOf(Math.round(((material.getPrice()*count)*(1+(isNds?(nds/100.0):0)+(isPercent?percent/100:0)))*100)/100.0)));
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });
        projectId = getIntent().getIntExtra(IContract.ID, 0);

        setCustomTitle(R.string.calc_add);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        ConfirmationDialog.show(this, 1, getResources().getString(R.string.warning), getResources().getString(R.string.calc_close_confirm));
    }

    @Override
    public void onConfirmed(int messageId) {
        Intent intent = new Intent(CalcAddActivity.this, ProjectEditActivity.class);
        intent.putExtra(IContract.ID, projectId);
        startActivity(intent);
        finish();
    }
}
