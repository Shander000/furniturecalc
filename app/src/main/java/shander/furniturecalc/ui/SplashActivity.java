package shander.furniturecalc.ui;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.concurrent.Executor;

import androidx.work.ListenableWorker;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import shander.furniturecalc.CalcApp;
import shander.furniturecalc.R;
import shander.furniturecalc.db.DBHelper;
import shander.furniturecalc.entities.BaseCatalogue;
import shander.furniturecalc.entities.Project;
import shander.furniturecalc.interfaces.IContract;
import shander.furniturecalc.util.IabHelper;
import shander.furniturecalc.util.IabResult;
import shander.furniturecalc.util.Inventory;
import shander.furniturecalc.utils.JSONHelper;
import shander.furniturecalc.workers.DBWorker;

public class SplashActivity extends AppCompatActivity {

    private static final long PROCEED_DELAY_MILLIS = 3000;

    private View mContentView;

    private View coordinator;

    private IabHelper mHelper;

    private Handler mHandler = new Handler();
    private Runnable mProceedRunnable = new Runnable() {
        @Override
        public void run() {
            goToMainActivity();
        }
    };
    private boolean isDbLoaded = false;
    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        coordinator = findViewById(R.id.myCoordinatorLayout);

        if (!CalcApp.getApplication().getPrefs().getBoolean(IContract.PREMIUM_ZERO, false)) {
            mHelper = new IabHelper(this, CalcApp.getApplication().getBase64Key());

            mContentView = findViewById(R.id.fullscreen_content);
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
            mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                public void onIabSetupFinished(IabResult result) {
                    if (!result.isSuccess()) {
                        Log.wtf("BILLING", "Problem setting up In-app Billing: " + result);
                    } else {
                        try {
                            mHelper.queryInventoryAsync(mGotInventoryListener);
                        } catch (IabHelper.IabAsyncInProgressException e) {
                            Log.wtf("QUERY", e.getMessage());
                        }
                    }
                }
            });
        }

        dbHelper = CalcApp.getApplication().getDbHelper();

        String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 1 );
        } else {
            checkDb();
            mHandler.postDelayed(mProceedRunnable, PROCEED_DELAY_MILLIS);
        }
    }

    public void checkDb() {
        File projects = new File(JSONHelper.FILE_PROJECTS_NAME);
        if (projects.exists()) {
            ArrayList<Project> backupProjects = (ArrayList<Project>) JSONHelper.importFromJSON(this, JSONHelper.ItemClass.Project);
            if (backupProjects != null) {
                if (backupProjects.size() != 0) {
                    for (Project p : backupProjects) {
                        dbHelper.saveProject(p);
                    }
                }
                projects.delete();
            }
        }
//        File bases = new File(JSONHelper.FILE_BASE_NAME);
//        if (bases.exists()) {
//            ArrayList<BaseCatalogue> backupBases = (ArrayList<BaseCatalogue>) JSONHelper.importFromJSON(this, JSONHelper.ItemClass.BaseCatalogue);
//            if (backupBases != null) {
//                if (backupBases.size() != 0) {
//                    for (BaseCatalogue p : backupBases) {
//                        dbHelper.saveCategory(p);
//                    }
//                    bases.delete();
//                    CalcApp.getApplication().getEditor().putBoolean("hasDb", true).apply();
//                }
//            }
//        }
        if (!CalcApp.getApplication().getPrefs().getBoolean("hasDb", false)) {
            WorkManager.getInstance().enqueue(OneTimeWorkRequest.from(DBWorker.class)).getResult()
                    .addListener(new Runnable() {
                                     @Override
                                     public void run() {
                                         runOnUiThread(new Runnable() {
                                             @Override
                                             public void run() {
                                                 SplashActivity.this.goToMainActivity();
                                             }
                                         });
                                     }
                                 }, new CurrentThreadExecutor());
        }
    }

    class CurrentThreadExecutor implements Executor {
        public void execute(@NonNull Runnable r) {
            r.run();
        }
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkDb();
            } else {
                Snackbar.make(coordinator, R.string.perm_rationale, Snackbar.LENGTH_LONG)
                        .setAction("Разрешить", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
                                ActivityCompat.requestPermissions(SplashActivity.this, PERMISSIONS, 1 );
                            }
                        })
                        .show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private IabHelper.QueryInventoryFinishedListener mGotInventoryListener
            = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {

            if (result.isFailure()) {
                Log.wtf("REQUEST", result.getMessage());
            }
            else {
//                try {
//                    mHelper.consumeAsync(inventory.getPurchase(IContract.PREMIUM_ZERO),
//                            null);
//                } catch (IabHelper.IabAsyncInProgressException e) {
//                    e.printStackTrace();
//                }
//                Log.wtf("PURCH", inventory.getPurchase(IContract.PREMIUM_ZERO).getPurchaseState() + "|" +
//                        inventory.getPurchase(IContract.PREMIUM_ZERO).getSignature());
                CalcApp.getApplication().getEditor().putBoolean(IContract.PREMIUM_ZERO, inventory.hasPurchase(IContract.PREMIUM_ZERO) &&
                        (inventory.getPurchase(IContract.PREMIUM_ZERO).getPurchaseState() == 0)).apply();
            }
        }
    };

    @Override
    protected void onPause() {
        mHandler.removeCallbacks(mProceedRunnable);

        super.onPause();
    }

    public void goToMainActivity() {
        if (mHelper != null) {
            try {
                mHelper.dispose();
            } catch (IabHelper.IabAsyncInProgressException e) {
                e.printStackTrace();
            }
        }
        mHelper = null;

            finish();

            Intent intent = new Intent(SplashActivity.this, LaunchActivity.class);
            startActivity(intent);
    }
}
