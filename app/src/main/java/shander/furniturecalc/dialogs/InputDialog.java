package shander.furniturecalc.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import shander.furniturecalc.R;

public class InputDialog extends DialogFragment {

    private final static String MESSAGE_ID_PARAM = "message_id";
    private final static String COMMENT_FIELD = "comment";

    private Listener mListener;
    private EditText mCommentTextView;

    public static void show(FragmentActivity fragmentActivity, int messageId, String comment) {
        FragmentManager manager = fragmentActivity.getSupportFragmentManager();
        InputDialog myDialogFragment = new InputDialog();
        Bundle params = new Bundle();
        params.putString(COMMENT_FIELD, comment);
        params.putInt(MESSAGE_ID_PARAM, messageId);
        myDialogFragment.setArguments(params);
        myDialogFragment.show(manager, "TimeDialog");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View contentView = inflater.inflate(R.layout.comment_dlg, null);

        mCommentTextView = contentView.findViewById(R.id.comment);

        builder.setView(contentView)
                .setTitle("Новый каталог")
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (mListener != null) {
                            mListener.onInputEnter(mCommentTextView.getText().toString());
                        }
                    }
                }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Listener) {
            mListener = (Listener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mListener = null;
    }

    public interface Listener {
        void onInputEnter(String comment);
    }
}
