package shander.furniturecalc.workers;

import android.content.Context;
import android.support.annotation.NonNull;

import androidx.work.Worker;
import androidx.work.WorkerParameters;
import shander.furniturecalc.CalcApp;
import shander.furniturecalc.db.DBHelper;
import shander.furniturecalc.ui.SplashActivity;

public class DBWorker extends Worker {

    public DBWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        DBHelper dbHelper = CalcApp.getApplication().getDbHelper();
        dbHelper.loadDbFromXls("default_db.xls", true);
        CalcApp.getApplication().getEditor().putBoolean("hasDb", true).apply();
        return Result.success();
    }
}
