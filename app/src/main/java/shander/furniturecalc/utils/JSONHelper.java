package shander.furniturecalc.utils;


import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import shander.furniturecalc.entities.BaseCatalogue;
import shander.furniturecalc.entities.Calculation;
import shander.furniturecalc.entities.Project;
import shander.furniturecalc.interfaces.IContract;

public class JSONHelper {

    public static final String FILE_PROJECTS_NAME = "projects.json";
    public static final String FILE_BASE_NAME = "bases.json";
    public static final String FILE_COUNTS_NAME = "counts.json";

    public static boolean exportToJSON(Context context, ArrayList<?> dataList) {

        Log.wtf("CALLED", "+");

        Gson gson = new Gson();
        DataItems dataItems = new DataItems();
        dataItems.setItems(dataList);
        String jsonString = gson.toJson(dataItems);

        FileOutputStream fileOutputStream = null;

        try {
            if (dataList.get(0) instanceof Integer) {
                if (dataList.get(0) == IContract.PROJECT_NULL) {
                    context.deleteFile(FILE_PROJECTS_NAME);
                }
            } else if (dataList.get(0) instanceof Project) {
                fileOutputStream = context.openFileOutput(FILE_PROJECTS_NAME, Context.MODE_PRIVATE);
            } else if (dataList.get(0) instanceof BaseCatalogue) {
                fileOutputStream = context.openFileOutput(FILE_BASE_NAME, Context.MODE_PRIVATE);
                Log.wtf("FOS!=NULL", String.valueOf(fileOutputStream != null));
            } else if (dataList.get(0) instanceof Calculation) {
                fileOutputStream = context.openFileOutput(FILE_COUNTS_NAME, Context.MODE_PRIVATE);
            }
            if (fileOutputStream != null) {
                Log.wtf("FOS!=NULL", "+");
                fileOutputStream.write(jsonString.getBytes());
                Log.wtf("STRING", jsonString);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return false;
    }

    public static ArrayList<?> importFromJSON(Context context, ItemClass type) {

        InputStreamReader streamReader = null;
        FileInputStream fileInputStream = null;
        try{
            switch (type) {
                case Project:
                    fileInputStream = context.openFileInput(FILE_PROJECTS_NAME);
                    break;
                case BaseCatalogue:
                    fileInputStream = context.openFileInput(FILE_BASE_NAME);
                    break;
                case Calculation:
                    fileInputStream = context.openFileInput(FILE_COUNTS_NAME);
                    break;
            }
            if (fileInputStream != null) {
                streamReader = new InputStreamReader(fileInputStream);
            }
            Gson gson = new Gson();
            if (streamReader != null) {
                switch (type) {
                    case Project:
                        ProjectItems projectItems = gson.fromJson(streamReader, ProjectItems.class);
                        if (projectItems != null) return projectItems.getProjects();
                        break;
                    case BaseCatalogue:
                        BaseItems baseItems = gson.fromJson(streamReader, BaseItems.class);
                        if (baseItems != null) {
                            return baseItems.getCatalogues();
                        }
                        break;
                    case Calculation:
                        CalculationItems calculationItems = gson.fromJson(streamReader, CalculationItems.class);
                        if (calculationItems != null) return calculationItems.getCalculations();
                }
            }
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
        finally {
            if (streamReader != null) {
                try {
                    streamReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    private static class CalculationItems {
        private ArrayList<Calculation> items;
        ArrayList<Calculation> getCalculations() {
            return items;
        }
        void setCalculations(ArrayList<Calculation> calculations) {
            this.items = calculations;
        }
    }

    public static class BaseItems {
        private ArrayList<BaseCatalogue> items;
        public ArrayList<BaseCatalogue> getCatalogues() {
            return items;
        }
        void setCatalogues(ArrayList<BaseCatalogue> catalogues) {
            this.items = catalogues;
        }
    }

    private static class ProjectItems {
        private ArrayList<Project> items;
        ArrayList<Project> getProjects() {
            return items;
        }
        void setProjects(ArrayList<Project> projects) {
            this.items = projects;
        }
    }

    private static class DataItems {
        private ArrayList<?> items;

        ArrayList<?> getItems() {
            return items;
        }
        void setItems(ArrayList<?> items) {
            this.items = items;
        }
    }

    public static enum ItemClass { BaseCatalogue, Calculation, Project}
}