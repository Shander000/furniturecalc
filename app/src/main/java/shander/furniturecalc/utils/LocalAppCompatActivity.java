package shander.furniturecalc.utils;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;
import android.widget.TextView;

import shander.furniturecalc.CalcApp;
import shander.furniturecalc.R;
import shander.furniturecalc.interfaces.IContract;


public class LocalAppCompatActivity extends AppCompatActivity {

    protected Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (CalcApp.getApplication().getPrefs().getBoolean(IContract.KEEP_SCREEN, false)) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
    }

    protected int getLayoutResource() {
        return 0;
    }

    protected void setCustomTitle(String title) {
        TextView titleTv = (TextView) toolbar.findViewById(R.id.title_custom);
        titleTv.setText(title);
    }

    protected void setCustomTitle(int resId) {
        TextView titleTv = (TextView) toolbar.findViewById(R.id.title_custom);
        titleTv.setText(getResources().getString(resId));
    }

    protected void configureToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

}
