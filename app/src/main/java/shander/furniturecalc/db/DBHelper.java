package shander.furniturecalc.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import shander.furniturecalc.CalcApp;
import shander.furniturecalc.entities.BaseCatalogue;
import shander.furniturecalc.entities.Calculation;
import shander.furniturecalc.entities.Material;
import shander.furniturecalc.entities.NotAvailableMaterial;
import shander.furniturecalc.entities.Project;
import shander.furniturecalc.interfaces.IContract;
import shander.furniturecalc.interfaces.IConverter;

public class DBHelper extends SQLiteOpenHelper implements IContract{

    public DBHelper(Context context, int version) {
        super(context, DB_NAME, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table " + TABLE_CATEGORIES + "(" + TABLE_ID +
                " integer primary key not null," + CATEGORY_NAME + " text);");

        sqLiteDatabase.execSQL("create table " + TABLE_MATERIALS + "(" + TABLE_ID +
                " integer primary key autoincrement not null," + MATERIAL_NAME + " text," +
                MATERIAL_ARTICLE + " text," + MATERIAL_PRICE + " real," + MATERIAL_COMMENT + " text," +
                MATERIAL_CATEGORY + " integer," + MATERIAL_UNIT + " text" + ");");

        sqLiteDatabase.execSQL("create table " + TABLE_CALCULATIONS + "(" + TABLE_ID +
                " integer primary key autoincrement not null," + CALCULATION_TITLE + " text," +
                CALCULATION_AMOUNT + " real," + CALCULATION_PROJECT_ID + " integer," + CALCULATION_MATERIAL_ID + " integer," +
                CALCULATION_COMMENT + " text," + CALCULATION_IS_NDS + " integer default 0," + CALCULATION_IS_PERCENT + " integer default 0," +
                CALCULATION_NDS + " integer," + CALCULATION_PERCENTS + " real," + MATERIAL_IS_AVAILABLE + " integer default 0" +");");

        sqLiteDatabase.execSQL("create table " + TABLE_NOT_AVAILABLE_MATEIALS + "(" + TABLE_ID +
                " integer primary key autoincrement not null," + NOT_AVAILABLE_MATERIAL + " integer,"
                + NOT_AVAILABLE_PROJECT + " integer," + NOT_AVAILABLE_AMOUNT + " real"+ ");");

        sqLiteDatabase.execSQL("create table " + TABLE_PROJECTS + "(" + TABLE_ID +
                " integer primary key autoincrement not null," + PROJECT_NAME + " text," +
                PROJECT_ADDRESS + " text," + PROJECT_FIO + " text," + PROJECT_NOTE + " text," +
                PROJECT_PHONE + " text" + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void wipeDB() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MATERIALS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CALCULATIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROJECTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOT_AVAILABLE_MATEIALS);

        onCreate(db);
//        setDefaultGroups(db);
//        setDefaultMelodies(db);
//        setDefaultSettings(db);
    }

    private IConverter<Cursor, Material> CURSOR_TO_MATERIAL_CONVERTER = new IConverter<Cursor, Material>() {
        @Override
        public Material convert(Cursor src) {
            return new Material(src.getString(src.getColumnIndex(MATERIAL_ARTICLE)),
                    src.getString(src.getColumnIndex(MATERIAL_COMMENT)),
                    src.getInt(src.getColumnIndex(TABLE_ID)),
                    src.getDouble(src.getColumnIndex(MATERIAL_PRICE)),
                    src.getString(src.getColumnIndex(MATERIAL_NAME)),
                    src.getInt(src.getColumnIndex(MATERIAL_CATEGORY)),
                    src.getString(src.getColumnIndex(MATERIAL_UNIT)));
        }
    };

    private IConverter<BaseCatalogue, ContentValues> CATEGORY_TO_CONTENT_CONVERTER = new IConverter<BaseCatalogue, ContentValues>() {
        @Override
        public ContentValues convert(BaseCatalogue src) {
            ContentValues cv = new ContentValues();
            if (src.getCatId() != 0) {
                cv.put(TABLE_ID, src.getCatId());
            }
            cv.put(CATEGORY_NAME, src.getName());
            return cv;
        }
    };

    private IConverter<Cursor, BaseCatalogue> CURSOR_TO_CATEGORY_CONVERTER = new IConverter<Cursor, BaseCatalogue>() {
        @Override
        public BaseCatalogue convert(Cursor src) {
            BaseCatalogue catalogue = new BaseCatalogue();

            int catId = src.getInt(src.getColumnIndex(TABLE_ID));
            SQLiteDatabase db = DBHelper.this.getWritableDatabase();
            String qString;
            qString = MATERIAL_CATEGORY +
                    " = " + "'" + catId + "'";
            catalogue.setCatId(catId);
            catalogue.setName(src.getString(src.getColumnIndex(CATEGORY_NAME)));
            Log.wtf("GETTING CAT:", catalogue.getName() + "|" + catalogue.getCatId());
            Cursor c = db.query(TABLE_MATERIALS, ALL_MATERIAL_FIELDS, qString, null, null, null, null);
            if (c.getCount() != 0) {
                c.moveToFirst();
                ArrayList<Material> materials = new ArrayList<>();
                for (int i = 0; i < c.getCount(); i++) {
                    Material material = CURSOR_TO_MATERIAL_CONVERTER.convert(c);
                    materials.add(material);
                    c.moveToNext();
                }
                catalogue.setMaterials(materials);
            }
            return catalogue;
        }
    };

    private IConverter<Calculation, ContentValues> CALCULATION_TO_CONTENT_CONVERTER = new IConverter<Calculation, ContentValues>() {
        @Override
        public ContentValues convert(Calculation src) {
            ContentValues cv = new ContentValues();
            if (src.getuId() != 0) {
                cv.put(TABLE_ID, src.getuId());
            }
            cv.put(CALCULATION_TITLE, src.getTitle());
            cv.put(CALCULATION_AMOUNT, src.getAmount());
            cv.put(CALCULATION_PROJECT_ID, src.getProjectId());
            cv.put(CALCULATION_MATERIAL_ID, src.getMaterial().getId());
            cv.put(CALCULATION_COMMENT, src.getComment());
            cv.put(CALCULATION_IS_NDS, src.getIsNds()?1:0);
            cv.put(CALCULATION_IS_PERCENT, src.getIsPercent()?1:0);
            cv.put(CALCULATION_NDS, src.getNds());
            cv.put(CALCULATION_PERCENTS, src.getPercent());
            cv.put(MATERIAL_IS_AVAILABLE, src.isMaterialAvailable()?1:0);
            return cv;
        }
    };

    private IConverter<Cursor, Calculation> CURSOR_TO_CALCULATION_CONVERTER = new IConverter<Cursor, Calculation>() {
        @Override
        public Calculation convert(Cursor src) {
            Calculation calculation = new Calculation();
            int materialId = src.getInt(src.getColumnIndex(CALCULATION_MATERIAL_ID));
            SQLiteDatabase db = DBHelper.this.getWritableDatabase();
            String qString;
            qString = TABLE_ID +
                    " = " + "'" + materialId + "'";
            Cursor c = db.query(TABLE_MATERIALS, ALL_MATERIAL_FIELDS, qString, null, null, null, null);
            c.moveToFirst();
            Material material = CURSOR_TO_MATERIAL_CONVERTER.convert(c);
            calculation.setMaterial(material);
            Double amount = src.getDouble(src.getColumnIndex(CALCULATION_AMOUNT));
            calculation.setAmount(amount);
            calculation.setComment(material.getComment());
            calculation.setProjectId(src.getInt(src.getColumnIndex(CALCULATION_PROJECT_ID)));
            boolean isNds = src.getInt(src.getColumnIndex(CALCULATION_IS_NDS)) == 1;
            boolean isPercent = src.getInt(src.getColumnIndex(CALCULATION_IS_PERCENT)) == 1;
            calculation.setIsNds(isNds);
            calculation.setIsPercent(isPercent);
            int percent = src.getInt(src.getColumnIndex(CALCULATION_PERCENTS));
            int nds = src.getInt(src.getColumnIndex(CALCULATION_NDS));
            calculation.setNds(nds);
            calculation.setPercent(percent);
            calculation.setTitle(src.getString(src.getColumnIndex(CALCULATION_TITLE)));
            calculation.setuId(src.getInt(src.getColumnIndex(TABLE_ID)));
            calculation.setMaterialAvailable(src.getInt(src.getColumnIndex(MATERIAL_IS_AVAILABLE)) == 1);
            calculation.setFullPrice(Math.round(((material.getPrice()*amount)*(1+(isNds?(nds/100.0):0)+(isPercent?(percent/100.0):0)))*100)/100.0);
            return calculation;
        }
    };

    private IConverter<Material, ContentValues> MATERIAL_TO_CONTENT_CONVERTER = new IConverter<Material, ContentValues>() {
        @Override
        public ContentValues convert(Material src) {
            ContentValues cv= new ContentValues();
            if (src.getId() != 0) {
                cv.put(TABLE_ID, src.getId());
            }
            cv.put(MATERIAL_ARTICLE, src.getArticle());
            cv.put(MATERIAL_COMMENT, src.getComment());
            cv.put(MATERIAL_PRICE, src.getPrice());
            cv.put(MATERIAL_NAME, src.getTitle());
            cv.put(MATERIAL_CATEGORY, src.getType());
            cv.put(MATERIAL_UNIT, src.getUnit());
            return cv;
        }
    };

    private IConverter<Project, ContentValues> PROJECT_TO_CONTENT_CONVERTER = new IConverter<Project, ContentValues>() {
        @Override
        public ContentValues convert(Project src) {
            ContentValues cv= new ContentValues();
            if (src.getId() != 0) {
                cv.put(TABLE_ID, src.getId());
            }
            cv.put(PROJECT_ADDRESS, src.getmAddress());
            cv.put(PROJECT_FIO, src.getmFIO());
            cv.put(PROJECT_NAME, src.getmName());
            cv.put(PROJECT_NOTE, src.getmNote());
            cv.put(PROJECT_PHONE, src.getmPhone());
            return cv;
        }
    };

    private IConverter<Cursor, Project> CURSOR_TO_PROJECT_CONVERTER = new IConverter<Cursor, Project>() {
        @Override
        public Project convert(Cursor src) {
            Project project = new Project();
            project.setId(src.getInt(src.getColumnIndex(TABLE_ID)));
            project.setmAddress(src.getString(src.getColumnIndex(PROJECT_ADDRESS)));
            project.setmFIO(src.getString(src.getColumnIndex(PROJECT_FIO)));
            project.setmName(src.getString(src.getColumnIndex(PROJECT_NAME)));
            project.setmNote(src.getString(src.getColumnIndex(PROJECT_NOTE)));
            project.setmPhone(src.getString(src.getColumnIndex(PROJECT_PHONE)));
            ArrayList<Calculation> calculations = new ArrayList<>();
            SQLiteDatabase db = DBHelper.this.getWritableDatabase();
            String qString = CALCULATION_PROJECT_ID +
                    " = " + "'" + project.getId() + "'";
            Cursor c = db.query(TABLE_CALCULATIONS, ALL_CALCULATION_FIELDS, qString, null, null, null, null);
            c.moveToFirst();
            if (c.getCount() != 0) {
                for (int i = 0; i < c.getCount(); i++) {
                    calculations.add(CURSOR_TO_CALCULATION_CONVERTER.convert(c));
                    c.moveToNext();
                }
            }
            project.setCalculations(calculations);
            Double cost = 0d;
            if (calculations.size() != 0) {
                for (Calculation calculation : calculations) {
                    cost += calculation.getFullPrice();
                }
            }
            project.setmCost(cost);
            return project;
        }
    };

    private IConverter<NotAvailableMaterial, ContentValues> NOT_AVAILABLE_TO_CONTENT_CONVERTER = new IConverter<NotAvailableMaterial, ContentValues>() {
        @Override
        public ContentValues convert(NotAvailableMaterial src) {
            ContentValues cv = new ContentValues();
            if (src.getId() != 0) {
                cv.put(TABLE_ID, src.getId());
            }
            Log.wtf("NAM ID", src.getId() + "");
            cv.put(NOT_AVAILABLE_AMOUNT, src.getCount());
            cv.put(NOT_AVAILABLE_MATERIAL, src.getMaterial().getId());
            cv.put(NOT_AVAILABLE_PROJECT, src.getProjectId());
            return cv;
        }
    };

    private IConverter<Cursor, NotAvailableMaterial> CURSOR_TO_NOT_AVAILABLE_CONVERTER = new IConverter<Cursor, NotAvailableMaterial>() {
        @Override
        public NotAvailableMaterial convert(Cursor src) {
            NotAvailableMaterial notAvailableMaterial = new NotAvailableMaterial();
            notAvailableMaterial.setId(src.getInt(src.getColumnIndex(TABLE_ID)));
            notAvailableMaterial.setCount(src.getDouble(src.getColumnIndex(NOT_AVAILABLE_AMOUNT)));
            notAvailableMaterial.setProjectId(src.getInt(src.getColumnIndex(NOT_AVAILABLE_PROJECT)));
            SQLiteDatabase db = DBHelper.this.getWritableDatabase();
            String qString;
            qString = TABLE_ID +
                    " = " + "'" + src.getInt(src.getColumnIndex(NOT_AVAILABLE_MATERIAL)) + "'";
            Cursor c = db.query(TABLE_MATERIALS, ALL_MATERIAL_FIELDS, qString, null, null, null, null);
            c.moveToFirst();
            notAvailableMaterial.setMaterial(CURSOR_TO_MATERIAL_CONVERTER.convert(c));
            return notAvailableMaterial;
        }
    };

    public ArrayList<BaseCatalogue> getAllCategories() {
        Cursor c = this.getWritableDatabase().query(TABLE_CATEGORIES, ALL_CATEGORY_FIELDS, null, null, null, null, null);
        c.moveToFirst();
        ArrayList<BaseCatalogue> catalogues = new ArrayList<>();
        for (int i = 0; i < c.getCount(); i++) {
            catalogues.add(CURSOR_TO_CATEGORY_CONVERTER.convert(c));
            c.moveToNext();
        }
        c.close();
        return catalogues;
    }

    public Calculation getSingleCalculation(int id) {
        String qString = TABLE_ID +
                " = " + "'" + id + "'";
        Cursor c = this.getWritableDatabase().query(TABLE_CALCULATIONS, ALL_CALCULATION_FIELDS, qString, null, null, null, null);
        c.moveToFirst();
        Calculation calculation = CURSOR_TO_CALCULATION_CONVERTER.convert(c);
        c.close();
        return calculation;
    }

    public ArrayList<Calculation> getAllCalculations() {
        Cursor c = this.getWritableDatabase().query(TABLE_CALCULATIONS, ALL_CALCULATION_FIELDS, null, null, null, null, null);
        c.moveToFirst();
        ArrayList<Calculation> calculations = new ArrayList<>();
        while (!c.isAfterLast()){
            calculations.add(CURSOR_TO_CALCULATION_CONVERTER.convert(c));
            c.moveToNext();
        }
        c.close();
        return calculations;
    }

    public NotAvailableMaterial getSingleNotAvailable(int id) {
        String qString = TABLE_ID +
                " = " + "'" + id + "'";
        Cursor c = this.getWritableDatabase().query(TABLE_NOT_AVAILABLE_MATEIALS, ALL_NOT_AVAILABLE_FIELDS, qString, null, null, null, null);
        c.moveToFirst();
        NotAvailableMaterial material = CURSOR_TO_NOT_AVAILABLE_CONVERTER.convert(c);
        c.close();
        return material;
    }

    public ArrayList<NotAvailableMaterial> getAllNotAvailable() {
        Cursor c = this.getWritableDatabase().query(TABLE_NOT_AVAILABLE_MATEIALS, ALL_NOT_AVAILABLE_FIELDS, null, null, null, null, null);
        c.moveToFirst();
        ArrayList<NotAvailableMaterial> materials = new ArrayList<>();
        if (c.getCount() != 0) {
            for (int i = 0; i < c.getCount(); i++){
                materials.add(CURSOR_TO_NOT_AVAILABLE_CONVERTER.convert(c));
                c.moveToNext();
            }
            Log.wtf("GANA", materials.size() + "");
        }
        c.close();
        return materials;
    }

    public ArrayList<Project> getProjectsWithNA() {
        ArrayList<Project> projects = new ArrayList<>();
        Log.wtf("DBH getPWithNA", "+");
        ArrayList<NotAvailableMaterial> materials = getAllNotAvailable();
        for (NotAvailableMaterial material : materials) {
            Log.wtf("DBH getPWithNA", material.getProjectId() + "|" + material.getMaterial().getId() + "|" + material.getMaterial().getTitle() + "|" + material.getCount());
            projects.add(getSingleProject(material.getProjectId()));
        }
        return projects;
    }

    public Project getSingleProject(int id) {
        String qString = TABLE_ID +
                " = " + "'" + id + "'";
        Cursor c = this.getWritableDatabase().query(TABLE_PROJECTS, ALL_PROJECT_FIELDS, qString, null, null, null, null);
        c.moveToFirst();
        Project project = CURSOR_TO_PROJECT_CONVERTER.convert(c);
        c.close();
        return project;
    }

    public ArrayList<Project> getAllProjects() {
        Cursor c = this.getWritableDatabase().query(TABLE_PROJECTS, ALL_PROJECT_FIELDS, null, null, null, null, null);
        c.moveToFirst();
        ArrayList<Project> materials = new ArrayList<>();
        if (c.getCount() != 0) {
            while (!c.isAfterLast()) {
                materials.add(CURSOR_TO_PROJECT_CONVERTER.convert(c));
                c.moveToNext();
            }
        }
        c.close();
        return materials;
    }

    public void saveCategory(BaseCatalogue catalogue) {
        this.getWritableDatabase().insertWithOnConflict(TABLE_CATEGORIES, null,
                CATEGORY_TO_CONTENT_CONVERTER.convert(catalogue), SQLiteDatabase.CONFLICT_REPLACE);
        if (catalogue.getMaterials() != null) {
            if (catalogue.getMaterials().size() != 0) {
                for (Material material : catalogue.getMaterials()) {
                    this.getWritableDatabase().insertWithOnConflict(TABLE_MATERIALS, null,
                            MATERIAL_TO_CONTENT_CONVERTER.convert(material), SQLiteDatabase.CONFLICT_REPLACE);
                }
            }
        }
    }

    public void saveProject(Project project) {
        this.getWritableDatabase().insertWithOnConflict(TABLE_PROJECTS, null,
                PROJECT_TO_CONTENT_CONVERTER.convert(project), SQLiteDatabase.CONFLICT_REPLACE);
//        if (project.getCalculations() != null) {
//            if (project.getCalculations().size()!=0) {
//                for (Calculation calculation : project.getCalculations()) {
//                    this.getWritableDatabase().insertWithOnConflict(TABLE_CALCULATIONS, null,
//                            CALCULATION_TO_CONTENT_CONVERTER.convert(calculation), SQLiteDatabase.CONFLICT_REPLACE);
//                }
//            }
//        }
    }

    public void saveCalculation(Calculation calculation) {
        this.getWritableDatabase().insertWithOnConflict(TABLE_CALCULATIONS, null,
                CALCULATION_TO_CONTENT_CONVERTER.convert(calculation), SQLiteDatabase.CONFLICT_REPLACE);
        if (!calculation.isMaterialAvailable()) {
            NotAvailableMaterial material = new NotAvailableMaterial();
            material.setProjectId(calculation.getProjectId());
            material.setCount(calculation.getAmount());
            material.setMaterial(calculation.getMaterial());
            material.setId(calculation.getMaterial().getTitle().hashCode() + calculation.getProjectId() + calculation.getuId());
            saveNotAvailable(material);
        }
    }

    public void saveMaterial(Material material) {
        this.getWritableDatabase().insertWithOnConflict(TABLE_MATERIALS, null,
                MATERIAL_TO_CONTENT_CONVERTER.convert(material), SQLiteDatabase.CONFLICT_REPLACE);
    }

    private void saveNotAvailable(NotAvailableMaterial material) {
        this.getWritableDatabase().insertWithOnConflict(TABLE_NOT_AVAILABLE_MATEIALS, null,
                NOT_AVAILABLE_TO_CONTENT_CONVERTER.convert(material), SQLiteDatabase.CONFLICT_REPLACE);
    }

    public void deleteCategory(int id) {
        if (this.getWritableDatabase().delete(TABLE_CATEGORIES, TABLE_ID + '=' + id, null) > 0) {
            this.getWritableDatabase().delete(TABLE_MATERIALS, MATERIAL_CATEGORY + '=' + id, null);
        }
    }

    public void deleteCalculation(int id) {
        Calculation calculation = getSingleCalculation(id);
        if (!calculation.isMaterialAvailable()) {
            deleteNAFromCalc(calculation.getProjectId(), calculation.getMaterial().getId());
        }
        this.getWritableDatabase().delete(TABLE_CALCULATIONS, TABLE_ID + '=' + id, null);
    }

    public void deleteMaterial(int id) {
        this.getWritableDatabase().delete(TABLE_MATERIALS, TABLE_ID + '=' + id, null);
    }

    public boolean deleteNAFromMaterialId (int mId) {
        String qString = NOT_AVAILABLE_MATERIAL +
                " = " + "'" + mId + "'";
        Cursor c = this.getWritableDatabase().query(TABLE_NOT_AVAILABLE_MATEIALS, ALL_NOT_AVAILABLE_FIELDS, qString, null, null, null, null);
        c.moveToFirst();
        for (int i = 0; i < c.getCount(); i++) {
            deleteNotAvailable(c.getInt(c.getColumnIndex(TABLE_ID)));
            c.moveToNext();
        }
        c.close();
        return true;
    }

    public boolean deleteNotAvailable(int materialId) {
        NotAvailableMaterial material = getSingleNotAvailable(materialId);
        ContentValues cv = new ContentValues();
        cv.put(MATERIAL_IS_AVAILABLE, 1);
        final String[] whereArgs = {String.valueOf(material.getProjectId()), String.valueOf(material.getMaterial().getId()) };
        this.getWritableDatabase().update(TABLE_CALCULATIONS, cv, CALCULATION_PROJECT_ID + " =? AND " + CALCULATION_MATERIAL_ID + " =?", whereArgs);
        return this.getWritableDatabase().delete(TABLE_NOT_AVAILABLE_MATEIALS, TABLE_ID + '=' + materialId, null) > 0;
    }

    public void deleteNAFromCalc(int projectId, int materialId) {
        final String whereClause = NOT_AVAILABLE_PROJECT + " =? AND " + NOT_AVAILABLE_MATERIAL + " =?";
        final String[] whereArgs = {String.valueOf(projectId), String.valueOf(materialId) };
        this.getWritableDatabase().delete(TABLE_NOT_AVAILABLE_MATEIALS, whereClause, whereArgs);
    }

    public void deleteProject(int id) {
        Project p = getSingleProject(id);
        ArrayList<Calculation> calculations = p.getCalculations();
        for (Calculation calculation : calculations) {
            deleteCalculation(calculation.getuId());
        }
        this.getWritableDatabase().delete(TABLE_PROJECTS, TABLE_ID + '=' + id, null);
    }

    public void loadDbFromXls(String filename, boolean isWipe) {
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "db.xls");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (isWipe) {
            AssetManager am = CalcApp.getApplication().getAssets();
            try {
                writeBytesToFile(am.open("default_db.xls"), file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                writeBytesToFile(new FileInputStream (filename), file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            readFromExcel(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readFromExcel(File file) throws IOException {
        HSSFWorkbook myExcelBook = new HSSFWorkbook(new FileInputStream(file));
        for (int i = 0; i < myExcelBook.getNumberOfSheets(); i++) {
            HSSFSheet myExcelSheet = myExcelBook.getSheetAt(i);
            int materialTitleNum = 0;
            int materialPriceNum = 1;
            int materialUnitNum = 2;
            int materialArticleNum = 3;
            int materialCommentNum = 4;
            BaseCatalogue catalogue = new BaseCatalogue();
            catalogue.setName(myExcelSheet.getSheetName());
            catalogue.setCatId(i+1);
            ArrayList<Material> materials = new ArrayList<>();
            for (Row row : myExcelSheet) {
                // iterate on cells for the current row
                Iterator<Cell> cellIterator = row.cellIterator();
                Material material = new Material();

                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    if (row.getRowNum() == 0) {
                        switch (cell.getStringCellValue()) {
                            case "Наименование":
                                materialTitleNum = cell.getColumnIndex();
                                break;
                            case "Цена":
                                materialPriceNum = cell.getColumnIndex();
                                break;
                            case "Ед.изм":
                                materialUnitNum = cell.getColumnIndex();
                                break;
                            case "Артикул":
                                materialArticleNum = cell.getColumnIndex();
                                break;
                            case "Примечание":
                                materialCommentNum = cell.getColumnIndex();
                                break;
                        }
                    } else {
                        if (cell.getColumnIndex() == materialTitleNum) {
                            material.setTitle(cell.getStringCellValue());
                        } else if (cell.getColumnIndex() == materialPriceNum) {
                            material.setPrice(cell.getNumericCellValue());
                        } else if (cell.getColumnIndex() == materialUnitNum) {
                            material.setUnit(cell.getStringCellValue());
                        } else if (cell.getColumnIndex() == materialArticleNum) {
                            material.setArticle(cell.getStringCellValue());
                        } else if (cell.getColumnIndex() == materialCommentNum) {
                            material.setComment(cell.getStringCellValue());
                        }
                    }
                }

                if (!material.getTitle().equals("") && !material.getArticle().equals("") &&
                        !material.getUnit().equals("") && material.getPrice() != 0) {
                    material.setType(i + 1);
                    materials.add(material);
                }
            }
            catalogue.setMaterials(materials);
            saveCategory(catalogue);
        }

        myExcelBook.close();

    }

    private void writeBytesToFile(InputStream is, File file) throws IOException{
        FileOutputStream fos = null;
        try {
            byte[] data = new byte[2048];
            int nbread = 0;
            fos = new FileOutputStream(file);
            while((nbread=is.read(data))>-1){
                fos.write(data,0,nbread);
            }
        }
        catch (Exception ex) {
            Log.wtf("FILE ERR", ex.getMessage());
        }
        finally{
            if (fos!=null){
                fos.close();
            }
        }
    }

}
